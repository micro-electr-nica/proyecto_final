`timescale 1ns / 1ps

module counter #(
    parameter M0 = 3'b00, 
    parameter M1 = 3'b01, 
    parameter M2 = 3'b10, 
    parameter M3 = 3'b11
    )(
    input CLK, ENABLE, RESET, CLK_HALF,
    input [2:0] MODO,
    input [3:0] D,
    output reg [3:0] Q,
    output reg RCO, LOAD); 
    always @(posedge CLK)begin
        if ((!ENABLE && !RESET) || RESET || !ENABLE) begin
            Q <= 'b0;
            RCO <= 'b0;
            LOAD <= 'b0;
        end
        else begin
            if (ENABLE) begin
                case(MODO)
                    M0: begin
                        Q <= Q+1;
                        LOAD <= 'b0;
                        if (Q == 4'hF) begin
                            //@(posedge CLK_HALF) RCO <= 1;
                            RCO <= 1;
                        end else begin
                            RCO <= 0;
                        end
                    end
                    M1: begin
                        Q <= Q-1;
                        LOAD <= 'b0;
                        if (Q == 4'h0) begin
                            //@(posedge CLK_HALF) RCO <= 1;
                            RCO <= 1;
                        end else begin
                            RCO <= 'b0;
                        end
                    end
                    M2: begin
                        Q <= Q-3;
                        LOAD <= 'b0;
                        if (Q == 4'h2 | Q == 4'h0 | Q == 4'h1) begin
                            //@(posedge CLK_HALF) RCO <= 'b1;
                            RCO <= 1;
                        end else begin
                            RCO <= 'b0;
                        end
                    end
                    M3: begin
                        Q <= D;
                        RCO <= 'b0;
                        LOAD <= 'b1;
                    end
                    default: begin
                        Q <= 'b0;
                        RCO <= 'b0;
                        LOAD <= 'b0;
                    end
                endcase
            end
        end
    end
endmodule