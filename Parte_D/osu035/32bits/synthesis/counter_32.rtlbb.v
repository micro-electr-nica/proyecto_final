module counter_32 (CLK, ENABLE, RESET, CLK_HALF, MODO[0], MODO[1], MODO[2], D[0], D[1], D[2], D[3], D[4], D[5], D[6], D[7], D[8], D[9], D[10], D[11], D[12], D[13], D[14], D[15], D[16], D[17], D[18], D[19], D[20], D[21], D[22], D[23], D[24], D[25], D[26], D[27], D[28], D[29], D[30], D[31], Q[0], Q[1], Q[2], Q[3], Q[4], Q[5], Q[6], Q[7], Q[8], Q[9], Q[10], Q[11], Q[12], Q[13], Q[14], Q[15], Q[16], Q[17], Q[18], Q[19], Q[20], Q[21], Q[22], Q[23], Q[24], Q[25], Q[26], Q[27], Q[28], Q[29], Q[30], Q[31], RCO, LOAD);

input CLK;
input ENABLE;
input RESET;
input CLK_HALF;
input MODO[0];
input MODO[1];
input MODO[2];
input D[0];
input D[1];
input D[2];
input D[3];
input D[4];
input D[5];
input D[6];
input D[7];
input D[8];
input D[9];
input D[10];
input D[11];
input D[12];
input D[13];
input D[14];
input D[15];
input D[16];
input D[17];
input D[18];
input D[19];
input D[20];
input D[21];
input D[22];
input D[23];
input D[24];
input D[25];
input D[26];
input D[27];
input D[28];
input D[29];
input D[30];
input D[31];
output Q[0];
output Q[1];
output Q[2];
output Q[3];
output Q[4];
output Q[5];
output Q[6];
output Q[7];
output Q[8];
output Q[9];
output Q[10];
output Q[11];
output Q[12];
output Q[13];
output Q[14];
output Q[15];
output Q[16];
output Q[17];
output Q[18];
output Q[19];
output Q[20];
output Q[21];
output Q[22];
output Q[23];
output Q[24];
output Q[25];
output Q[26];
output Q[27];
output Q[28];
output Q[29];
output Q[30];
output Q[31];
output RCO;
output LOAD;

BUFX4 BUFX4_1 ( .A(MODO1_1_), .Y(MODO1_1_bF_buf3_) );
BUFX2 BUFX2_1 ( .A(MODO1_1_), .Y(MODO1_1_bF_buf2_) );
BUFX2 BUFX2_2 ( .A(MODO1_1_), .Y(MODO1_1_bF_buf1_) );
BUFX2 BUFX2_3 ( .A(MODO1_1_), .Y(MODO1_1_bF_buf0_) );
BUFX4 BUFX4_2 ( .A(MODO1_0_), .Y(MODO1_0_bF_buf4_) );
BUFX4 BUFX4_3 ( .A(MODO1_0_), .Y(MODO1_0_bF_buf3_) );
BUFX4 BUFX4_4 ( .A(MODO1_0_), .Y(MODO1_0_bF_buf2_) );
BUFX2 BUFX2_4 ( .A(MODO1_0_), .Y(MODO1_0_bF_buf1_) );
BUFX2 BUFX2_5 ( .A(MODO1_0_), .Y(MODO1_0_bF_buf0_) );
INVX1 INVX1_1 ( .A(MODO[0]), .Y(_0_) );
NOR2X1 NOR2X1_1 ( .A(MODO[2]), .B(_0_), .Y(MODO0_0_) );
INVX1 INVX1_2 ( .A(MODO[1]), .Y(_1_) );
NOR2X1 NOR2X1_2 ( .A(MODO[2]), .B(_1_), .Y(MODO0_1_) );
NAND2X1 NAND2X1_1 ( .A(LOAD1), .B(LOAD0), .Y(_2_) );
NAND2X1 NAND2X1_2 ( .A(LOAD2), .B(LOAD3), .Y(_3_) );
NOR2X1 NOR2X1_3 ( .A(_2_), .B(_3_), .Y(_4_) );
NAND2X1 NAND2X1_3 ( .A(LOAD4), .B(LOAD6), .Y(_5_) );
NAND2X1 NAND2X1_4 ( .A(LOAD5), .B(LOAD7), .Y(_6_) );
NOR2X1 NOR2X1_4 ( .A(_5_), .B(_6_), .Y(_7_) );
AND2X2 AND2X2_1 ( .A(_4_), .B(_7_), .Y(_20_) );
NAND2X1 NAND2X1_5 ( .A(RCO3), .B(RCO2), .Y(_8_) );
NAND2X1 NAND2X1_6 ( .A(RCO4), .B(RCO5), .Y(_9_) );
NAND2X1 NAND2X1_7 ( .A(RCO6), .B(RCO7), .Y(_10_) );
NOR3X1 NOR3X1_1 ( .A(_8_), .B(_9_), .C(_10_), .Y(_22_) );
INVX4 INVX4_1 ( .A(CLK), .Y(_11_) );
AOI21X1 AOI21X1_1 ( .A(MODO[0]), .B(MODO[1]), .C(MODO[2]), .Y(_12_) );
NAND2X1 NAND2X1_8 ( .A(RCO0), .B(_12_), .Y(_13_) );
OAI21X1 OAI21X1_1 ( .A(_11_), .B(_12_), .C(_13_), .Y(clk1) );
NAND2X1 NAND2X1_9 ( .A(RCO1), .B(_12_), .Y(_14_) );
OAI21X1 OAI21X1_2 ( .A(_11_), .B(_12_), .C(_14_), .Y(clk2) );
NAND2X1 NAND2X1_10 ( .A(RCO2), .B(_12_), .Y(_15_) );
OAI21X1 OAI21X1_3 ( .A(_11_), .B(_12_), .C(_15_), .Y(clk3) );
NAND2X1 NAND2X1_11 ( .A(RCO3), .B(_12_), .Y(_16_) );
OAI21X1 OAI21X1_4 ( .A(_11_), .B(_12_), .C(_16_), .Y(clk4) );
NAND2X1 NAND2X1_12 ( .A(RCO4), .B(_12_), .Y(_17_) );
OAI21X1 OAI21X1_5 ( .A(_11_), .B(_12_), .C(_17_), .Y(clk5) );
NAND2X1 NAND2X1_13 ( .A(RCO5), .B(_12_), .Y(_18_) );
OAI21X1 OAI21X1_6 ( .A(_11_), .B(_12_), .C(_18_), .Y(clk6) );
NAND2X1 NAND2X1_14 ( .A(RCO6), .B(_12_), .Y(_19_) );
OAI21X1 OAI21X1_7 ( .A(_11_), .B(_12_), .C(_19_), .Y(clk7) );
AOI21X1 AOI21X1_2 ( .A(_0_), .B(_1_), .C(MODO[2]), .Y(MODO1_0_) );
AND2X2 AND2X2_2 ( .A(MODO0_0_), .B(MODO[1]), .Y(MODO1_1_) );
BUFX2 BUFX2_6 ( .A(MODO[2]), .Y(MODO0_2_) );
BUFX4 BUFX4_5 ( .A(MODO[2]), .Y(MODO1_2_) );
BUFX2 BUFX2_7 ( .A(_20_), .Y(LOAD) );
BUFX2 BUFX2_8 ( .A(_21__0_), .Y(Q[0]) );
BUFX2 BUFX2_9 ( .A(_21__1_), .Y(Q[1]) );
BUFX2 BUFX2_10 ( .A(_21__2_), .Y(Q[2]) );
BUFX2 BUFX2_11 ( .A(_21__3_), .Y(Q[3]) );
BUFX2 BUFX2_12 ( .A(_21__4_), .Y(Q[4]) );
BUFX2 BUFX2_13 ( .A(_21__5_), .Y(Q[5]) );
BUFX2 BUFX2_14 ( .A(_21__6_), .Y(Q[6]) );
BUFX2 BUFX2_15 ( .A(_21__7_), .Y(Q[7]) );
BUFX2 BUFX2_16 ( .A(_21__8_), .Y(Q[8]) );
BUFX2 BUFX2_17 ( .A(_21__9_), .Y(Q[9]) );
BUFX2 BUFX2_18 ( .A(_21__10_), .Y(Q[10]) );
BUFX2 BUFX2_19 ( .A(_21__11_), .Y(Q[11]) );
BUFX2 BUFX2_20 ( .A(_21__12_), .Y(Q[12]) );
BUFX2 BUFX2_21 ( .A(_21__13_), .Y(Q[13]) );
BUFX2 BUFX2_22 ( .A(_21__14_), .Y(Q[14]) );
BUFX2 BUFX2_23 ( .A(_21__15_), .Y(Q[15]) );
BUFX2 BUFX2_24 ( .A(_21__16_), .Y(Q[16]) );
BUFX2 BUFX2_25 ( .A(_21__17_), .Y(Q[17]) );
BUFX2 BUFX2_26 ( .A(_21__18_), .Y(Q[18]) );
BUFX2 BUFX2_27 ( .A(_21__19_), .Y(Q[19]) );
BUFX2 BUFX2_28 ( .A(_21__20_), .Y(Q[20]) );
BUFX2 BUFX2_29 ( .A(_21__21_), .Y(Q[21]) );
BUFX2 BUFX2_30 ( .A(_21__22_), .Y(Q[22]) );
BUFX2 BUFX2_31 ( .A(_21__23_), .Y(Q[23]) );
BUFX2 BUFX2_32 ( .A(_21__24_), .Y(Q[24]) );
BUFX2 BUFX2_33 ( .A(_21__25_), .Y(Q[25]) );
BUFX2 BUFX2_34 ( .A(_21__26_), .Y(Q[26]) );
BUFX2 BUFX2_35 ( .A(_21__27_), .Y(Q[27]) );
BUFX2 BUFX2_36 ( .A(_21__28_), .Y(Q[28]) );
BUFX2 BUFX2_37 ( .A(_21__29_), .Y(Q[29]) );
BUFX2 BUFX2_38 ( .A(_21__30_), .Y(Q[30]) );
BUFX2 BUFX2_39 ( .A(_21__31_), .Y(Q[31]) );
BUFX2 BUFX2_40 ( .A(_22_), .Y(RCO) );
INVX1 INVX1_3 ( .A(RESET), .Y(_26_) );
NAND2X1 NAND2X1_15 ( .A(ENABLE), .B(_26_), .Y(_27_) );
INVX1 INVX1_4 ( .A(MODO0_1_), .Y(_28_) );
INVX2 INVX2_1 ( .A(MODO0_2_), .Y(_29_) );
NAND3X1 NAND3X1_1 ( .A(MODO0_0_), .B(_28_), .C(_29_), .Y(_30_) );
NOR2X1 NOR2X1_5 ( .A(_21__1_), .B(_21__0_), .Y(_31_) );
NOR2X1 NOR2X1_6 ( .A(_21__3_), .B(_21__2_), .Y(_32_) );
NAND2X1 NAND2X1_16 ( .A(_31_), .B(_32_), .Y(_33_) );
OR2X2 OR2X2_1 ( .A(_33_), .B(_30_), .Y(_34_) );
INVX1 INVX1_5 ( .A(MODO0_0_), .Y(_35_) );
NAND3X1 NAND3X1_2 ( .A(_35_), .B(_28_), .C(_29_), .Y(_36_) );
INVX1 INVX1_6 ( .A(_36_), .Y(_37_) );
INVX1 INVX1_7 ( .A(_21__3_), .Y(_38_) );
NAND3X1 NAND3X1_3 ( .A(_21__2_), .B(_21__1_), .C(_21__0_), .Y(_39_) );
NOR2X1 NOR2X1_7 ( .A(_38_), .B(_39_), .Y(_40_) );
NAND3X1 NAND3X1_4 ( .A(MODO0_1_), .B(_35_), .C(_29_), .Y(_41_) );
INVX1 INVX1_8 ( .A(_41_), .Y(_42_) );
AOI21X1 AOI21X1_3 ( .A(_21__1_), .B(_21__0_), .C(_21__2_), .Y(_43_) );
INVX1 INVX1_9 ( .A(_43_), .Y(_44_) );
NOR2X1 NOR2X1_8 ( .A(_21__3_), .B(_44_), .Y(_45_) );
AOI22X1 AOI22X1_1 ( .A(_37_), .B(_40_), .C(_42_), .D(_45_), .Y(_46_) );
AOI21X1 AOI21X1_4 ( .A(_46_), .B(_34_), .C(_27_), .Y(_25_) );
AND2X2 AND2X2_3 ( .A(MODO0_0_), .B(MODO0_1_), .Y(_47_) );
NAND2X1 NAND2X1_17 ( .A(_29_), .B(_47_), .Y(_48_) );
NOR2X1 NOR2X1_9 ( .A(_27_), .B(_48_), .Y(_23_) );
NAND2X1 NAND2X1_18 ( .A(D[0]), .B(_23_), .Y(_49_) );
INVX1 INVX1_10 ( .A(_21__0_), .Y(_50_) );
NOR2X1 NOR2X1_10 ( .A(MODO0_0_), .B(MODO0_2_), .Y(_51_) );
INVX1 INVX1_11 ( .A(_30_), .Y(_52_) );
OAI21X1 OAI21X1_8 ( .A(_51_), .B(_52_), .C(_50_), .Y(_53_) );
OAI21X1 OAI21X1_9 ( .A(_27_), .B(_53_), .C(_49_), .Y(_24__0_) );
INVX1 INVX1_12 ( .A(_51_), .Y(_54_) );
XNOR2X1 XNOR2X1_1 ( .A(_21__1_), .B(_21__0_), .Y(_55_) );
OR2X2 OR2X2_2 ( .A(_55_), .B(_54_), .Y(_56_) );
INVX1 INVX1_13 ( .A(_48_), .Y(_57_) );
AOI22X1 AOI22X1_2 ( .A(_52_), .B(_55_), .C(D[1]), .D(_57_), .Y(_58_) );
AOI21X1 AOI21X1_5 ( .A(_58_), .B(_56_), .C(_27_), .Y(_24__1_) );
INVX1 INVX1_14 ( .A(_39_), .Y(_59_) );
NOR2X1 NOR2X1_11 ( .A(_43_), .B(_59_), .Y(_60_) );
AOI22X1 AOI22X1_3 ( .A(_57_), .B(D[2]), .C(_37_), .D(_60_), .Y(_61_) );
NAND2X1 NAND2X1_19 ( .A(_39_), .B(_44_), .Y(_62_) );
INVX1 INVX1_15 ( .A(_21__2_), .Y(_63_) );
XNOR2X1 XNOR2X1_2 ( .A(_31_), .B(_63_), .Y(_64_) );
AOI22X1 AOI22X1_4 ( .A(_62_), .B(_42_), .C(_52_), .D(_64_), .Y(_65_) );
AOI21X1 AOI21X1_6 ( .A(_65_), .B(_61_), .C(_27_), .Y(_24__2_) );
NAND3X1 NAND3X1_5 ( .A(_29_), .B(D[3]), .C(_47_), .Y(_66_) );
OAI21X1 OAI21X1_10 ( .A(_30_), .B(_33_), .C(_66_), .Y(_67_) );
AOI21X1 AOI21X1_7 ( .A(_42_), .B(_45_), .C(_67_), .Y(_68_) );
NAND2X1 NAND2X1_20 ( .A(_21__3_), .B(_59_), .Y(_69_) );
AOI21X1 AOI21X1_8 ( .A(_38_), .B(_39_), .C(_36_), .Y(_70_) );
NOR3X1 NOR3X1_2 ( .A(_21__2_), .B(_21__1_), .C(_21__0_), .Y(_71_) );
OAI22X1 OAI22X1_1 ( .A(_71_), .B(_30_), .C(_41_), .D(_43_), .Y(_72_) );
AOI22X1 AOI22X1_5 ( .A(_70_), .B(_69_), .C(_21__3_), .D(_72_), .Y(_73_) );
AOI21X1 AOI21X1_9 ( .A(_68_), .B(_73_), .C(_27_), .Y(_24__3_) );
DFFPOSX1 DFFPOSX1_1 ( .CLK(CLK), .D(_24__0_), .Q(_21__0_) );
DFFPOSX1 DFFPOSX1_2 ( .CLK(CLK), .D(_24__1_), .Q(_21__1_) );
DFFPOSX1 DFFPOSX1_3 ( .CLK(CLK), .D(_24__2_), .Q(_21__2_) );
DFFPOSX1 DFFPOSX1_4 ( .CLK(CLK), .D(_24__3_), .Q(_21__3_) );
DFFPOSX1 DFFPOSX1_5 ( .CLK(CLK), .D(_23_), .Q(LOAD0) );
DFFPOSX1 DFFPOSX1_6 ( .CLK(CLK), .D(_25_), .Q(RCO0) );
INVX1 INVX1_16 ( .A(RESET), .Y(_77_) );
NAND2X1 NAND2X1_21 ( .A(ENABLE), .B(_77_), .Y(_78_) );
INVX1 INVX1_17 ( .A(MODO1_1_bF_buf3_), .Y(_79_) );
INVX2 INVX2_2 ( .A(MODO1_2_), .Y(_80_) );
NAND3X1 NAND3X1_6 ( .A(MODO1_0_bF_buf4_), .B(_79_), .C(_80_), .Y(_81_) );
NOR2X1 NOR2X1_12 ( .A(_21__5_), .B(_21__4_), .Y(_82_) );
NOR2X1 NOR2X1_13 ( .A(_21__7_), .B(_21__6_), .Y(_83_) );
NAND2X1 NAND2X1_22 ( .A(_82_), .B(_83_), .Y(_84_) );
OR2X2 OR2X2_3 ( .A(_84_), .B(_81_), .Y(_85_) );
INVX1 INVX1_18 ( .A(MODO1_0_bF_buf3_), .Y(_86_) );
NAND3X1 NAND3X1_7 ( .A(_86_), .B(_79_), .C(_80_), .Y(_87_) );
INVX1 INVX1_19 ( .A(_87_), .Y(_88_) );
INVX1 INVX1_20 ( .A(_21__7_), .Y(_89_) );
NAND3X1 NAND3X1_8 ( .A(_21__6_), .B(_21__5_), .C(_21__4_), .Y(_90_) );
NOR2X1 NOR2X1_14 ( .A(_89_), .B(_90_), .Y(_91_) );
NAND3X1 NAND3X1_9 ( .A(MODO1_1_bF_buf2_), .B(_86_), .C(_80_), .Y(_92_) );
INVX1 INVX1_21 ( .A(_92_), .Y(_93_) );
AOI21X1 AOI21X1_10 ( .A(_21__5_), .B(_21__4_), .C(_21__6_), .Y(_94_) );
INVX1 INVX1_22 ( .A(_94_), .Y(_95_) );
NOR2X1 NOR2X1_15 ( .A(_21__7_), .B(_95_), .Y(_96_) );
AOI22X1 AOI22X1_6 ( .A(_88_), .B(_91_), .C(_93_), .D(_96_), .Y(_97_) );
AOI21X1 AOI21X1_11 ( .A(_97_), .B(_85_), .C(_78_), .Y(_76_) );
AND2X2 AND2X2_4 ( .A(MODO1_0_bF_buf2_), .B(MODO1_1_bF_buf1_), .Y(_98_) );
NAND2X1 NAND2X1_23 ( .A(_80_), .B(_98_), .Y(_99_) );
NOR2X1 NOR2X1_16 ( .A(_78_), .B(_99_), .Y(_74_) );
NAND2X1 NAND2X1_24 ( .A(D[4]), .B(_74_), .Y(_100_) );
INVX1 INVX1_23 ( .A(_21__4_), .Y(_101_) );
NOR2X1 NOR2X1_17 ( .A(MODO1_0_bF_buf1_), .B(MODO1_2_), .Y(_102_) );
INVX1 INVX1_24 ( .A(_81_), .Y(_103_) );
OAI21X1 OAI21X1_11 ( .A(_102_), .B(_103_), .C(_101_), .Y(_104_) );
OAI21X1 OAI21X1_12 ( .A(_78_), .B(_104_), .C(_100_), .Y(_75__0_) );
INVX1 INVX1_25 ( .A(_102_), .Y(_105_) );
XNOR2X1 XNOR2X1_3 ( .A(_21__5_), .B(_21__4_), .Y(_106_) );
OR2X2 OR2X2_4 ( .A(_106_), .B(_105_), .Y(_107_) );
INVX1 INVX1_26 ( .A(_99_), .Y(_108_) );
AOI22X1 AOI22X1_7 ( .A(_103_), .B(_106_), .C(D[5]), .D(_108_), .Y(_109_) );
AOI21X1 AOI21X1_12 ( .A(_109_), .B(_107_), .C(_78_), .Y(_75__1_) );
INVX1 INVX1_27 ( .A(_90_), .Y(_110_) );
NOR2X1 NOR2X1_18 ( .A(_94_), .B(_110_), .Y(_111_) );
AOI22X1 AOI22X1_8 ( .A(_108_), .B(D[6]), .C(_88_), .D(_111_), .Y(_112_) );
NAND2X1 NAND2X1_25 ( .A(_90_), .B(_95_), .Y(_113_) );
INVX1 INVX1_28 ( .A(_21__6_), .Y(_114_) );
XNOR2X1 XNOR2X1_4 ( .A(_82_), .B(_114_), .Y(_115_) );
AOI22X1 AOI22X1_9 ( .A(_113_), .B(_93_), .C(_103_), .D(_115_), .Y(_116_) );
AOI21X1 AOI21X1_13 ( .A(_116_), .B(_112_), .C(_78_), .Y(_75__2_) );
NAND3X1 NAND3X1_10 ( .A(_80_), .B(D[7]), .C(_98_), .Y(_117_) );
OAI21X1 OAI21X1_13 ( .A(_81_), .B(_84_), .C(_117_), .Y(_118_) );
AOI21X1 AOI21X1_14 ( .A(_93_), .B(_96_), .C(_118_), .Y(_119_) );
NAND2X1 NAND2X1_26 ( .A(_21__7_), .B(_110_), .Y(_120_) );
AOI21X1 AOI21X1_15 ( .A(_89_), .B(_90_), .C(_87_), .Y(_121_) );
NOR3X1 NOR3X1_3 ( .A(_21__6_), .B(_21__5_), .C(_21__4_), .Y(_122_) );
OAI22X1 OAI22X1_2 ( .A(_122_), .B(_81_), .C(_92_), .D(_94_), .Y(_123_) );
AOI22X1 AOI22X1_10 ( .A(_121_), .B(_120_), .C(_21__7_), .D(_123_), .Y(_124_) );
AOI21X1 AOI21X1_16 ( .A(_119_), .B(_124_), .C(_78_), .Y(_75__3_) );
DFFPOSX1 DFFPOSX1_7 ( .CLK(clk1), .D(_75__0_), .Q(_21__4_) );
DFFPOSX1 DFFPOSX1_8 ( .CLK(clk1), .D(_75__1_), .Q(_21__5_) );
DFFPOSX1 DFFPOSX1_9 ( .CLK(clk1), .D(_75__2_), .Q(_21__6_) );
DFFPOSX1 DFFPOSX1_10 ( .CLK(clk1), .D(_75__3_), .Q(_21__7_) );
DFFPOSX1 DFFPOSX1_11 ( .CLK(clk1), .D(_74_), .Q(LOAD1) );
DFFPOSX1 DFFPOSX1_12 ( .CLK(clk1), .D(_76_), .Q(RCO1) );
INVX1 INVX1_29 ( .A(RESET), .Y(_128_) );
NAND2X1 NAND2X1_27 ( .A(ENABLE), .B(_128_), .Y(_129_) );
INVX1 INVX1_30 ( .A(MODO1_1_bF_buf0_), .Y(_130_) );
INVX2 INVX2_3 ( .A(MODO1_2_), .Y(_131_) );
NAND3X1 NAND3X1_11 ( .A(MODO1_0_bF_buf0_), .B(_130_), .C(_131_), .Y(_132_) );
NOR2X1 NOR2X1_19 ( .A(_21__9_), .B(_21__8_), .Y(_133_) );
NOR2X1 NOR2X1_20 ( .A(_21__11_), .B(_21__10_), .Y(_134_) );
NAND2X1 NAND2X1_28 ( .A(_133_), .B(_134_), .Y(_135_) );
OR2X2 OR2X2_5 ( .A(_135_), .B(_132_), .Y(_136_) );
INVX1 INVX1_31 ( .A(MODO1_0_bF_buf4_), .Y(_137_) );
NAND3X1 NAND3X1_12 ( .A(_137_), .B(_130_), .C(_131_), .Y(_138_) );
INVX1 INVX1_32 ( .A(_138_), .Y(_139_) );
INVX1 INVX1_33 ( .A(_21__11_), .Y(_140_) );
NAND3X1 NAND3X1_13 ( .A(_21__10_), .B(_21__9_), .C(_21__8_), .Y(_141_) );
NOR2X1 NOR2X1_21 ( .A(_140_), .B(_141_), .Y(_142_) );
NAND3X1 NAND3X1_14 ( .A(MODO1_1_bF_buf3_), .B(_137_), .C(_131_), .Y(_143_) );
INVX1 INVX1_34 ( .A(_143_), .Y(_144_) );
AOI21X1 AOI21X1_17 ( .A(_21__9_), .B(_21__8_), .C(_21__10_), .Y(_145_) );
INVX1 INVX1_35 ( .A(_145_), .Y(_146_) );
NOR2X1 NOR2X1_22 ( .A(_21__11_), .B(_146_), .Y(_147_) );
AOI22X1 AOI22X1_11 ( .A(_139_), .B(_142_), .C(_144_), .D(_147_), .Y(_148_) );
AOI21X1 AOI21X1_18 ( .A(_148_), .B(_136_), .C(_129_), .Y(_127_) );
AND2X2 AND2X2_5 ( .A(MODO1_0_bF_buf3_), .B(MODO1_1_bF_buf2_), .Y(_149_) );
NAND2X1 NAND2X1_29 ( .A(_131_), .B(_149_), .Y(_150_) );
NOR2X1 NOR2X1_23 ( .A(_129_), .B(_150_), .Y(_125_) );
NAND2X1 NAND2X1_30 ( .A(D[8]), .B(_125_), .Y(_151_) );
INVX1 INVX1_36 ( .A(_21__8_), .Y(_152_) );
NOR2X1 NOR2X1_24 ( .A(MODO1_0_bF_buf2_), .B(MODO1_2_), .Y(_153_) );
INVX1 INVX1_37 ( .A(_132_), .Y(_154_) );
OAI21X1 OAI21X1_14 ( .A(_153_), .B(_154_), .C(_152_), .Y(_155_) );
OAI21X1 OAI21X1_15 ( .A(_129_), .B(_155_), .C(_151_), .Y(_126__0_) );
INVX1 INVX1_38 ( .A(_153_), .Y(_156_) );
XNOR2X1 XNOR2X1_5 ( .A(_21__9_), .B(_21__8_), .Y(_157_) );
OR2X2 OR2X2_6 ( .A(_157_), .B(_156_), .Y(_158_) );
INVX1 INVX1_39 ( .A(_150_), .Y(_159_) );
AOI22X1 AOI22X1_12 ( .A(_154_), .B(_157_), .C(D[9]), .D(_159_), .Y(_160_) );
AOI21X1 AOI21X1_19 ( .A(_160_), .B(_158_), .C(_129_), .Y(_126__1_) );
INVX1 INVX1_40 ( .A(_141_), .Y(_161_) );
NOR2X1 NOR2X1_25 ( .A(_145_), .B(_161_), .Y(_162_) );
AOI22X1 AOI22X1_13 ( .A(_159_), .B(D[10]), .C(_139_), .D(_162_), .Y(_163_) );
NAND2X1 NAND2X1_31 ( .A(_141_), .B(_146_), .Y(_164_) );
INVX1 INVX1_41 ( .A(_21__10_), .Y(_165_) );
XNOR2X1 XNOR2X1_6 ( .A(_133_), .B(_165_), .Y(_166_) );
AOI22X1 AOI22X1_14 ( .A(_164_), .B(_144_), .C(_154_), .D(_166_), .Y(_167_) );
AOI21X1 AOI21X1_20 ( .A(_167_), .B(_163_), .C(_129_), .Y(_126__2_) );
NAND3X1 NAND3X1_15 ( .A(_131_), .B(D[11]), .C(_149_), .Y(_168_) );
OAI21X1 OAI21X1_16 ( .A(_132_), .B(_135_), .C(_168_), .Y(_169_) );
AOI21X1 AOI21X1_21 ( .A(_144_), .B(_147_), .C(_169_), .Y(_170_) );
NAND2X1 NAND2X1_32 ( .A(_21__11_), .B(_161_), .Y(_171_) );
AOI21X1 AOI21X1_22 ( .A(_140_), .B(_141_), .C(_138_), .Y(_172_) );
NOR3X1 NOR3X1_4 ( .A(_21__10_), .B(_21__9_), .C(_21__8_), .Y(_173_) );
OAI22X1 OAI22X1_3 ( .A(_173_), .B(_132_), .C(_143_), .D(_145_), .Y(_174_) );
AOI22X1 AOI22X1_15 ( .A(_172_), .B(_171_), .C(_21__11_), .D(_174_), .Y(_175_) );
AOI21X1 AOI21X1_23 ( .A(_170_), .B(_175_), .C(_129_), .Y(_126__3_) );
DFFPOSX1 DFFPOSX1_13 ( .CLK(clk2), .D(_126__0_), .Q(_21__8_) );
DFFPOSX1 DFFPOSX1_14 ( .CLK(clk2), .D(_126__1_), .Q(_21__9_) );
DFFPOSX1 DFFPOSX1_15 ( .CLK(clk2), .D(_126__2_), .Q(_21__10_) );
DFFPOSX1 DFFPOSX1_16 ( .CLK(clk2), .D(_126__3_), .Q(_21__11_) );
DFFPOSX1 DFFPOSX1_17 ( .CLK(clk2), .D(_125_), .Q(LOAD2) );
DFFPOSX1 DFFPOSX1_18 ( .CLK(clk2), .D(_127_), .Q(RCO2) );
INVX1 INVX1_42 ( .A(RESET), .Y(_179_) );
NAND2X1 NAND2X1_33 ( .A(ENABLE), .B(_179_), .Y(_180_) );
INVX1 INVX1_43 ( .A(MODO1_1_bF_buf1_), .Y(_181_) );
INVX2 INVX2_4 ( .A(MODO1_2_), .Y(_182_) );
NAND3X1 NAND3X1_16 ( .A(MODO1_0_bF_buf1_), .B(_181_), .C(_182_), .Y(_183_) );
NOR2X1 NOR2X1_26 ( .A(_21__13_), .B(_21__12_), .Y(_184_) );
NOR2X1 NOR2X1_27 ( .A(_21__15_), .B(_21__14_), .Y(_185_) );
NAND2X1 NAND2X1_34 ( .A(_184_), .B(_185_), .Y(_186_) );
OR2X2 OR2X2_7 ( .A(_186_), .B(_183_), .Y(_187_) );
INVX1 INVX1_44 ( .A(MODO1_0_bF_buf0_), .Y(_188_) );
NAND3X1 NAND3X1_17 ( .A(_188_), .B(_181_), .C(_182_), .Y(_189_) );
INVX1 INVX1_45 ( .A(_189_), .Y(_190_) );
INVX1 INVX1_46 ( .A(_21__15_), .Y(_191_) );
NAND3X1 NAND3X1_18 ( .A(_21__14_), .B(_21__13_), .C(_21__12_), .Y(_192_) );
NOR2X1 NOR2X1_28 ( .A(_191_), .B(_192_), .Y(_193_) );
NAND3X1 NAND3X1_19 ( .A(MODO1_1_bF_buf0_), .B(_188_), .C(_182_), .Y(_194_) );
INVX1 INVX1_47 ( .A(_194_), .Y(_195_) );
AOI21X1 AOI21X1_24 ( .A(_21__13_), .B(_21__12_), .C(_21__14_), .Y(_196_) );
INVX1 INVX1_48 ( .A(_196_), .Y(_197_) );
NOR2X1 NOR2X1_29 ( .A(_21__15_), .B(_197_), .Y(_198_) );
AOI22X1 AOI22X1_16 ( .A(_190_), .B(_193_), .C(_195_), .D(_198_), .Y(_199_) );
AOI21X1 AOI21X1_25 ( .A(_199_), .B(_187_), .C(_180_), .Y(_178_) );
AND2X2 AND2X2_6 ( .A(MODO1_0_bF_buf4_), .B(MODO1_1_bF_buf3_), .Y(_200_) );
NAND2X1 NAND2X1_35 ( .A(_182_), .B(_200_), .Y(_201_) );
NOR2X1 NOR2X1_30 ( .A(_180_), .B(_201_), .Y(_176_) );
NAND2X1 NAND2X1_36 ( .A(D[12]), .B(_176_), .Y(_202_) );
INVX1 INVX1_49 ( .A(_21__12_), .Y(_203_) );
NOR2X1 NOR2X1_31 ( .A(MODO1_0_bF_buf3_), .B(MODO1_2_), .Y(_204_) );
INVX1 INVX1_50 ( .A(_183_), .Y(_205_) );
OAI21X1 OAI21X1_17 ( .A(_204_), .B(_205_), .C(_203_), .Y(_206_) );
OAI21X1 OAI21X1_18 ( .A(_180_), .B(_206_), .C(_202_), .Y(_177__0_) );
INVX1 INVX1_51 ( .A(_204_), .Y(_207_) );
XNOR2X1 XNOR2X1_7 ( .A(_21__13_), .B(_21__12_), .Y(_208_) );
OR2X2 OR2X2_8 ( .A(_208_), .B(_207_), .Y(_209_) );
INVX1 INVX1_52 ( .A(_201_), .Y(_210_) );
AOI22X1 AOI22X1_17 ( .A(_205_), .B(_208_), .C(D[13]), .D(_210_), .Y(_211_) );
AOI21X1 AOI21X1_26 ( .A(_211_), .B(_209_), .C(_180_), .Y(_177__1_) );
INVX1 INVX1_53 ( .A(_192_), .Y(_212_) );
NOR2X1 NOR2X1_32 ( .A(_196_), .B(_212_), .Y(_213_) );
AOI22X1 AOI22X1_18 ( .A(_210_), .B(D[14]), .C(_190_), .D(_213_), .Y(_214_) );
NAND2X1 NAND2X1_37 ( .A(_192_), .B(_197_), .Y(_215_) );
INVX1 INVX1_54 ( .A(_21__14_), .Y(_216_) );
XNOR2X1 XNOR2X1_8 ( .A(_184_), .B(_216_), .Y(_217_) );
AOI22X1 AOI22X1_19 ( .A(_215_), .B(_195_), .C(_205_), .D(_217_), .Y(_218_) );
AOI21X1 AOI21X1_27 ( .A(_218_), .B(_214_), .C(_180_), .Y(_177__2_) );
NAND3X1 NAND3X1_20 ( .A(_182_), .B(D[15]), .C(_200_), .Y(_219_) );
OAI21X1 OAI21X1_19 ( .A(_183_), .B(_186_), .C(_219_), .Y(_220_) );
AOI21X1 AOI21X1_28 ( .A(_195_), .B(_198_), .C(_220_), .Y(_221_) );
NAND2X1 NAND2X1_38 ( .A(_21__15_), .B(_212_), .Y(_222_) );
AOI21X1 AOI21X1_29 ( .A(_191_), .B(_192_), .C(_189_), .Y(_223_) );
NOR3X1 NOR3X1_5 ( .A(_21__14_), .B(_21__13_), .C(_21__12_), .Y(_224_) );
OAI22X1 OAI22X1_4 ( .A(_224_), .B(_183_), .C(_194_), .D(_196_), .Y(_225_) );
AOI22X1 AOI22X1_20 ( .A(_223_), .B(_222_), .C(_21__15_), .D(_225_), .Y(_226_) );
AOI21X1 AOI21X1_30 ( .A(_221_), .B(_226_), .C(_180_), .Y(_177__3_) );
DFFPOSX1 DFFPOSX1_19 ( .CLK(clk3), .D(_177__0_), .Q(_21__12_) );
DFFPOSX1 DFFPOSX1_20 ( .CLK(clk3), .D(_177__1_), .Q(_21__13_) );
DFFPOSX1 DFFPOSX1_21 ( .CLK(clk3), .D(_177__2_), .Q(_21__14_) );
DFFPOSX1 DFFPOSX1_22 ( .CLK(clk3), .D(_177__3_), .Q(_21__15_) );
DFFPOSX1 DFFPOSX1_23 ( .CLK(clk3), .D(_176_), .Q(LOAD3) );
DFFPOSX1 DFFPOSX1_24 ( .CLK(clk3), .D(_178_), .Q(RCO3) );
INVX1 INVX1_55 ( .A(RESET), .Y(_230_) );
NAND2X1 NAND2X1_39 ( .A(ENABLE), .B(_230_), .Y(_231_) );
INVX1 INVX1_56 ( .A(MODO1_1_bF_buf2_), .Y(_232_) );
INVX2 INVX2_5 ( .A(MODO1_2_), .Y(_233_) );
NAND3X1 NAND3X1_21 ( .A(MODO1_0_bF_buf2_), .B(_232_), .C(_233_), .Y(_234_) );
NOR2X1 NOR2X1_33 ( .A(_21__17_), .B(_21__16_), .Y(_235_) );
NOR2X1 NOR2X1_34 ( .A(_21__19_), .B(_21__18_), .Y(_236_) );
NAND2X1 NAND2X1_40 ( .A(_235_), .B(_236_), .Y(_237_) );
OR2X2 OR2X2_9 ( .A(_237_), .B(_234_), .Y(_238_) );
INVX1 INVX1_57 ( .A(MODO1_0_bF_buf1_), .Y(_239_) );
NAND3X1 NAND3X1_22 ( .A(_239_), .B(_232_), .C(_233_), .Y(_240_) );
INVX1 INVX1_58 ( .A(_240_), .Y(_241_) );
INVX1 INVX1_59 ( .A(_21__19_), .Y(_242_) );
NAND3X1 NAND3X1_23 ( .A(_21__18_), .B(_21__17_), .C(_21__16_), .Y(_243_) );
NOR2X1 NOR2X1_35 ( .A(_242_), .B(_243_), .Y(_244_) );
NAND3X1 NAND3X1_24 ( .A(MODO1_1_bF_buf1_), .B(_239_), .C(_233_), .Y(_245_) );
INVX1 INVX1_60 ( .A(_245_), .Y(_246_) );
AOI21X1 AOI21X1_31 ( .A(_21__17_), .B(_21__16_), .C(_21__18_), .Y(_247_) );
INVX1 INVX1_61 ( .A(_247_), .Y(_248_) );
NOR2X1 NOR2X1_36 ( .A(_21__19_), .B(_248_), .Y(_249_) );
AOI22X1 AOI22X1_21 ( .A(_241_), .B(_244_), .C(_246_), .D(_249_), .Y(_250_) );
AOI21X1 AOI21X1_32 ( .A(_250_), .B(_238_), .C(_231_), .Y(_229_) );
AND2X2 AND2X2_7 ( .A(MODO1_0_bF_buf0_), .B(MODO1_1_bF_buf0_), .Y(_251_) );
NAND2X1 NAND2X1_41 ( .A(_233_), .B(_251_), .Y(_252_) );
NOR2X1 NOR2X1_37 ( .A(_231_), .B(_252_), .Y(_227_) );
NAND2X1 NAND2X1_42 ( .A(D[16]), .B(_227_), .Y(_253_) );
INVX1 INVX1_62 ( .A(_21__16_), .Y(_254_) );
NOR2X1 NOR2X1_38 ( .A(MODO1_0_bF_buf4_), .B(MODO1_2_), .Y(_255_) );
INVX1 INVX1_63 ( .A(_234_), .Y(_256_) );
OAI21X1 OAI21X1_20 ( .A(_255_), .B(_256_), .C(_254_), .Y(_257_) );
OAI21X1 OAI21X1_21 ( .A(_231_), .B(_257_), .C(_253_), .Y(_228__0_) );
INVX1 INVX1_64 ( .A(_255_), .Y(_258_) );
XNOR2X1 XNOR2X1_9 ( .A(_21__17_), .B(_21__16_), .Y(_259_) );
OR2X2 OR2X2_10 ( .A(_259_), .B(_258_), .Y(_260_) );
INVX1 INVX1_65 ( .A(_252_), .Y(_261_) );
AOI22X1 AOI22X1_22 ( .A(_256_), .B(_259_), .C(D[17]), .D(_261_), .Y(_262_) );
AOI21X1 AOI21X1_33 ( .A(_262_), .B(_260_), .C(_231_), .Y(_228__1_) );
INVX1 INVX1_66 ( .A(_243_), .Y(_263_) );
NOR2X1 NOR2X1_39 ( .A(_247_), .B(_263_), .Y(_264_) );
AOI22X1 AOI22X1_23 ( .A(_261_), .B(D[18]), .C(_241_), .D(_264_), .Y(_265_) );
NAND2X1 NAND2X1_43 ( .A(_243_), .B(_248_), .Y(_266_) );
INVX1 INVX1_67 ( .A(_21__18_), .Y(_267_) );
XNOR2X1 XNOR2X1_10 ( .A(_235_), .B(_267_), .Y(_268_) );
AOI22X1 AOI22X1_24 ( .A(_266_), .B(_246_), .C(_256_), .D(_268_), .Y(_269_) );
AOI21X1 AOI21X1_34 ( .A(_269_), .B(_265_), .C(_231_), .Y(_228__2_) );
NAND3X1 NAND3X1_25 ( .A(_233_), .B(D[19]), .C(_251_), .Y(_270_) );
OAI21X1 OAI21X1_22 ( .A(_234_), .B(_237_), .C(_270_), .Y(_271_) );
AOI21X1 AOI21X1_35 ( .A(_246_), .B(_249_), .C(_271_), .Y(_272_) );
NAND2X1 NAND2X1_44 ( .A(_21__19_), .B(_263_), .Y(_273_) );
AOI21X1 AOI21X1_36 ( .A(_242_), .B(_243_), .C(_240_), .Y(_274_) );
NOR3X1 NOR3X1_6 ( .A(_21__18_), .B(_21__17_), .C(_21__16_), .Y(_275_) );
OAI22X1 OAI22X1_5 ( .A(_275_), .B(_234_), .C(_245_), .D(_247_), .Y(_276_) );
AOI22X1 AOI22X1_25 ( .A(_274_), .B(_273_), .C(_21__19_), .D(_276_), .Y(_277_) );
AOI21X1 AOI21X1_37 ( .A(_272_), .B(_277_), .C(_231_), .Y(_228__3_) );
DFFPOSX1 DFFPOSX1_25 ( .CLK(clk4), .D(_228__0_), .Q(_21__16_) );
DFFPOSX1 DFFPOSX1_26 ( .CLK(clk4), .D(_228__1_), .Q(_21__17_) );
DFFPOSX1 DFFPOSX1_27 ( .CLK(clk4), .D(_228__2_), .Q(_21__18_) );
DFFPOSX1 DFFPOSX1_28 ( .CLK(clk4), .D(_228__3_), .Q(_21__19_) );
DFFPOSX1 DFFPOSX1_29 ( .CLK(clk4), .D(_227_), .Q(LOAD4) );
DFFPOSX1 DFFPOSX1_30 ( .CLK(clk4), .D(_229_), .Q(RCO4) );
INVX1 INVX1_68 ( .A(RESET), .Y(_281_) );
NAND2X1 NAND2X1_45 ( .A(ENABLE), .B(_281_), .Y(_282_) );
INVX1 INVX1_69 ( .A(MODO1_1_bF_buf3_), .Y(_283_) );
INVX2 INVX2_6 ( .A(MODO1_2_), .Y(_284_) );
NAND3X1 NAND3X1_26 ( .A(MODO1_0_bF_buf3_), .B(_283_), .C(_284_), .Y(_285_) );
NOR2X1 NOR2X1_40 ( .A(_21__21_), .B(_21__20_), .Y(_286_) );
NOR2X1 NOR2X1_41 ( .A(_21__23_), .B(_21__22_), .Y(_287_) );
NAND2X1 NAND2X1_46 ( .A(_286_), .B(_287_), .Y(_288_) );
OR2X2 OR2X2_11 ( .A(_288_), .B(_285_), .Y(_289_) );
INVX1 INVX1_70 ( .A(MODO1_0_bF_buf2_), .Y(_290_) );
NAND3X1 NAND3X1_27 ( .A(_290_), .B(_283_), .C(_284_), .Y(_291_) );
INVX1 INVX1_71 ( .A(_291_), .Y(_292_) );
INVX1 INVX1_72 ( .A(_21__23_), .Y(_293_) );
NAND3X1 NAND3X1_28 ( .A(_21__22_), .B(_21__21_), .C(_21__20_), .Y(_294_) );
NOR2X1 NOR2X1_42 ( .A(_293_), .B(_294_), .Y(_295_) );
NAND3X1 NAND3X1_29 ( .A(MODO1_1_bF_buf2_), .B(_290_), .C(_284_), .Y(_296_) );
INVX1 INVX1_73 ( .A(_296_), .Y(_297_) );
AOI21X1 AOI21X1_38 ( .A(_21__21_), .B(_21__20_), .C(_21__22_), .Y(_298_) );
INVX1 INVX1_74 ( .A(_298_), .Y(_299_) );
NOR2X1 NOR2X1_43 ( .A(_21__23_), .B(_299_), .Y(_300_) );
AOI22X1 AOI22X1_26 ( .A(_292_), .B(_295_), .C(_297_), .D(_300_), .Y(_301_) );
AOI21X1 AOI21X1_39 ( .A(_301_), .B(_289_), .C(_282_), .Y(_280_) );
AND2X2 AND2X2_8 ( .A(MODO1_0_bF_buf1_), .B(MODO1_1_bF_buf1_), .Y(_302_) );
NAND2X1 NAND2X1_47 ( .A(_284_), .B(_302_), .Y(_303_) );
NOR2X1 NOR2X1_44 ( .A(_282_), .B(_303_), .Y(_278_) );
NAND2X1 NAND2X1_48 ( .A(D[20]), .B(_278_), .Y(_304_) );
INVX1 INVX1_75 ( .A(_21__20_), .Y(_305_) );
NOR2X1 NOR2X1_45 ( .A(MODO1_0_bF_buf0_), .B(MODO1_2_), .Y(_306_) );
INVX1 INVX1_76 ( .A(_285_), .Y(_307_) );
OAI21X1 OAI21X1_23 ( .A(_306_), .B(_307_), .C(_305_), .Y(_308_) );
OAI21X1 OAI21X1_24 ( .A(_282_), .B(_308_), .C(_304_), .Y(_279__0_) );
INVX1 INVX1_77 ( .A(_306_), .Y(_309_) );
XNOR2X1 XNOR2X1_11 ( .A(_21__21_), .B(_21__20_), .Y(_310_) );
OR2X2 OR2X2_12 ( .A(_310_), .B(_309_), .Y(_311_) );
INVX1 INVX1_78 ( .A(_303_), .Y(_312_) );
AOI22X1 AOI22X1_27 ( .A(_307_), .B(_310_), .C(D[21]), .D(_312_), .Y(_313_) );
AOI21X1 AOI21X1_40 ( .A(_313_), .B(_311_), .C(_282_), .Y(_279__1_) );
INVX1 INVX1_79 ( .A(_294_), .Y(_314_) );
NOR2X1 NOR2X1_46 ( .A(_298_), .B(_314_), .Y(_315_) );
AOI22X1 AOI22X1_28 ( .A(_312_), .B(D[22]), .C(_292_), .D(_315_), .Y(_316_) );
NAND2X1 NAND2X1_49 ( .A(_294_), .B(_299_), .Y(_317_) );
INVX1 INVX1_80 ( .A(_21__22_), .Y(_318_) );
XNOR2X1 XNOR2X1_12 ( .A(_286_), .B(_318_), .Y(_319_) );
AOI22X1 AOI22X1_29 ( .A(_317_), .B(_297_), .C(_307_), .D(_319_), .Y(_320_) );
AOI21X1 AOI21X1_41 ( .A(_320_), .B(_316_), .C(_282_), .Y(_279__2_) );
NAND3X1 NAND3X1_30 ( .A(_284_), .B(D[23]), .C(_302_), .Y(_321_) );
OAI21X1 OAI21X1_25 ( .A(_285_), .B(_288_), .C(_321_), .Y(_322_) );
AOI21X1 AOI21X1_42 ( .A(_297_), .B(_300_), .C(_322_), .Y(_323_) );
NAND2X1 NAND2X1_50 ( .A(_21__23_), .B(_314_), .Y(_324_) );
AOI21X1 AOI21X1_43 ( .A(_293_), .B(_294_), .C(_291_), .Y(_325_) );
NOR3X1 NOR3X1_7 ( .A(_21__22_), .B(_21__21_), .C(_21__20_), .Y(_326_) );
OAI22X1 OAI22X1_6 ( .A(_326_), .B(_285_), .C(_296_), .D(_298_), .Y(_327_) );
AOI22X1 AOI22X1_30 ( .A(_325_), .B(_324_), .C(_21__23_), .D(_327_), .Y(_328_) );
AOI21X1 AOI21X1_44 ( .A(_323_), .B(_328_), .C(_282_), .Y(_279__3_) );
DFFPOSX1 DFFPOSX1_31 ( .CLK(clk5), .D(_279__0_), .Q(_21__20_) );
DFFPOSX1 DFFPOSX1_32 ( .CLK(clk5), .D(_279__1_), .Q(_21__21_) );
DFFPOSX1 DFFPOSX1_33 ( .CLK(clk5), .D(_279__2_), .Q(_21__22_) );
DFFPOSX1 DFFPOSX1_34 ( .CLK(clk5), .D(_279__3_), .Q(_21__23_) );
DFFPOSX1 DFFPOSX1_35 ( .CLK(clk5), .D(_278_), .Q(LOAD5) );
DFFPOSX1 DFFPOSX1_36 ( .CLK(clk5), .D(_280_), .Q(RCO5) );
INVX1 INVX1_81 ( .A(RESET), .Y(_332_) );
NAND2X1 NAND2X1_51 ( .A(ENABLE), .B(_332_), .Y(_333_) );
INVX1 INVX1_82 ( .A(MODO1_1_bF_buf0_), .Y(_334_) );
INVX2 INVX2_7 ( .A(MODO1_2_), .Y(_335_) );
NAND3X1 NAND3X1_31 ( .A(MODO1_0_bF_buf4_), .B(_334_), .C(_335_), .Y(_336_) );
NOR2X1 NOR2X1_47 ( .A(_21__25_), .B(_21__24_), .Y(_337_) );
NOR2X1 NOR2X1_48 ( .A(_21__27_), .B(_21__26_), .Y(_338_) );
NAND2X1 NAND2X1_52 ( .A(_337_), .B(_338_), .Y(_339_) );
OR2X2 OR2X2_13 ( .A(_339_), .B(_336_), .Y(_340_) );
INVX1 INVX1_83 ( .A(MODO1_0_bF_buf3_), .Y(_341_) );
NAND3X1 NAND3X1_32 ( .A(_341_), .B(_334_), .C(_335_), .Y(_342_) );
INVX1 INVX1_84 ( .A(_342_), .Y(_343_) );
INVX1 INVX1_85 ( .A(_21__27_), .Y(_344_) );
NAND3X1 NAND3X1_33 ( .A(_21__26_), .B(_21__25_), .C(_21__24_), .Y(_345_) );
NOR2X1 NOR2X1_49 ( .A(_344_), .B(_345_), .Y(_346_) );
NAND3X1 NAND3X1_34 ( .A(MODO1_1_bF_buf3_), .B(_341_), .C(_335_), .Y(_347_) );
INVX1 INVX1_86 ( .A(_347_), .Y(_348_) );
AOI21X1 AOI21X1_45 ( .A(_21__25_), .B(_21__24_), .C(_21__26_), .Y(_349_) );
INVX1 INVX1_87 ( .A(_349_), .Y(_350_) );
NOR2X1 NOR2X1_50 ( .A(_21__27_), .B(_350_), .Y(_351_) );
AOI22X1 AOI22X1_31 ( .A(_343_), .B(_346_), .C(_348_), .D(_351_), .Y(_352_) );
AOI21X1 AOI21X1_46 ( .A(_352_), .B(_340_), .C(_333_), .Y(_331_) );
AND2X2 AND2X2_9 ( .A(MODO1_0_bF_buf2_), .B(MODO1_1_bF_buf2_), .Y(_353_) );
NAND2X1 NAND2X1_53 ( .A(_335_), .B(_353_), .Y(_354_) );
NOR2X1 NOR2X1_51 ( .A(_333_), .B(_354_), .Y(_329_) );
NAND2X1 NAND2X1_54 ( .A(D[24]), .B(_329_), .Y(_355_) );
INVX1 INVX1_88 ( .A(_21__24_), .Y(_356_) );
NOR2X1 NOR2X1_52 ( .A(MODO1_0_bF_buf1_), .B(MODO1_2_), .Y(_357_) );
INVX1 INVX1_89 ( .A(_336_), .Y(_358_) );
OAI21X1 OAI21X1_26 ( .A(_357_), .B(_358_), .C(_356_), .Y(_359_) );
OAI21X1 OAI21X1_27 ( .A(_333_), .B(_359_), .C(_355_), .Y(_330__0_) );
INVX1 INVX1_90 ( .A(_357_), .Y(_360_) );
XNOR2X1 XNOR2X1_13 ( .A(_21__25_), .B(_21__24_), .Y(_361_) );
OR2X2 OR2X2_14 ( .A(_361_), .B(_360_), .Y(_362_) );
INVX1 INVX1_91 ( .A(_354_), .Y(_363_) );
AOI22X1 AOI22X1_32 ( .A(_358_), .B(_361_), .C(D[25]), .D(_363_), .Y(_364_) );
AOI21X1 AOI21X1_47 ( .A(_364_), .B(_362_), .C(_333_), .Y(_330__1_) );
INVX1 INVX1_92 ( .A(_345_), .Y(_365_) );
NOR2X1 NOR2X1_53 ( .A(_349_), .B(_365_), .Y(_366_) );
AOI22X1 AOI22X1_33 ( .A(_363_), .B(D[26]), .C(_343_), .D(_366_), .Y(_367_) );
NAND2X1 NAND2X1_55 ( .A(_345_), .B(_350_), .Y(_368_) );
INVX1 INVX1_93 ( .A(_21__26_), .Y(_369_) );
XNOR2X1 XNOR2X1_14 ( .A(_337_), .B(_369_), .Y(_370_) );
AOI22X1 AOI22X1_34 ( .A(_368_), .B(_348_), .C(_358_), .D(_370_), .Y(_371_) );
AOI21X1 AOI21X1_48 ( .A(_371_), .B(_367_), .C(_333_), .Y(_330__2_) );
NAND3X1 NAND3X1_35 ( .A(_335_), .B(D[27]), .C(_353_), .Y(_372_) );
OAI21X1 OAI21X1_28 ( .A(_336_), .B(_339_), .C(_372_), .Y(_373_) );
AOI21X1 AOI21X1_49 ( .A(_348_), .B(_351_), .C(_373_), .Y(_374_) );
NAND2X1 NAND2X1_56 ( .A(_21__27_), .B(_365_), .Y(_375_) );
AOI21X1 AOI21X1_50 ( .A(_344_), .B(_345_), .C(_342_), .Y(_376_) );
NOR3X1 NOR3X1_8 ( .A(_21__26_), .B(_21__25_), .C(_21__24_), .Y(_377_) );
OAI22X1 OAI22X1_7 ( .A(_377_), .B(_336_), .C(_347_), .D(_349_), .Y(_378_) );
AOI22X1 AOI22X1_35 ( .A(_376_), .B(_375_), .C(_21__27_), .D(_378_), .Y(_379_) );
AOI21X1 AOI21X1_51 ( .A(_374_), .B(_379_), .C(_333_), .Y(_330__3_) );
DFFPOSX1 DFFPOSX1_37 ( .CLK(clk6), .D(_330__0_), .Q(_21__24_) );
DFFPOSX1 DFFPOSX1_38 ( .CLK(clk6), .D(_330__1_), .Q(_21__25_) );
DFFPOSX1 DFFPOSX1_39 ( .CLK(clk6), .D(_330__2_), .Q(_21__26_) );
DFFPOSX1 DFFPOSX1_40 ( .CLK(clk6), .D(_330__3_), .Q(_21__27_) );
DFFPOSX1 DFFPOSX1_41 ( .CLK(clk6), .D(_329_), .Q(LOAD6) );
DFFPOSX1 DFFPOSX1_42 ( .CLK(clk6), .D(_331_), .Q(RCO6) );
INVX1 INVX1_94 ( .A(RESET), .Y(_383_) );
NAND2X1 NAND2X1_57 ( .A(ENABLE), .B(_383_), .Y(_384_) );
INVX1 INVX1_95 ( .A(MODO1_1_bF_buf1_), .Y(_385_) );
INVX2 INVX2_8 ( .A(MODO1_2_), .Y(_386_) );
NAND3X1 NAND3X1_36 ( .A(MODO1_0_bF_buf0_), .B(_385_), .C(_386_), .Y(_387_) );
NOR2X1 NOR2X1_54 ( .A(_21__29_), .B(_21__28_), .Y(_388_) );
NOR2X1 NOR2X1_55 ( .A(_21__31_), .B(_21__30_), .Y(_389_) );
NAND2X1 NAND2X1_58 ( .A(_388_), .B(_389_), .Y(_390_) );
OR2X2 OR2X2_15 ( .A(_390_), .B(_387_), .Y(_391_) );
INVX1 INVX1_96 ( .A(MODO1_0_bF_buf4_), .Y(_392_) );
NAND3X1 NAND3X1_37 ( .A(_392_), .B(_385_), .C(_386_), .Y(_393_) );
INVX1 INVX1_97 ( .A(_393_), .Y(_394_) );
INVX1 INVX1_98 ( .A(_21__31_), .Y(_395_) );
NAND3X1 NAND3X1_38 ( .A(_21__30_), .B(_21__29_), .C(_21__28_), .Y(_396_) );
NOR2X1 NOR2X1_56 ( .A(_395_), .B(_396_), .Y(_397_) );
NAND3X1 NAND3X1_39 ( .A(MODO1_1_bF_buf0_), .B(_392_), .C(_386_), .Y(_398_) );
INVX1 INVX1_99 ( .A(_398_), .Y(_399_) );
AOI21X1 AOI21X1_52 ( .A(_21__29_), .B(_21__28_), .C(_21__30_), .Y(_400_) );
INVX1 INVX1_100 ( .A(_400_), .Y(_401_) );
NOR2X1 NOR2X1_57 ( .A(_21__31_), .B(_401_), .Y(_402_) );
AOI22X1 AOI22X1_36 ( .A(_394_), .B(_397_), .C(_399_), .D(_402_), .Y(_403_) );
AOI21X1 AOI21X1_53 ( .A(_403_), .B(_391_), .C(_384_), .Y(_382_) );
AND2X2 AND2X2_10 ( .A(MODO1_0_bF_buf3_), .B(MODO1_1_bF_buf3_), .Y(_404_) );
NAND2X1 NAND2X1_59 ( .A(_386_), .B(_404_), .Y(_405_) );
NOR2X1 NOR2X1_58 ( .A(_384_), .B(_405_), .Y(_380_) );
NAND2X1 NAND2X1_60 ( .A(D[28]), .B(_380_), .Y(_406_) );
INVX1 INVX1_101 ( .A(_21__28_), .Y(_407_) );
NOR2X1 NOR2X1_59 ( .A(MODO1_0_bF_buf2_), .B(MODO1_2_), .Y(_408_) );
INVX1 INVX1_102 ( .A(_387_), .Y(_409_) );
OAI21X1 OAI21X1_29 ( .A(_408_), .B(_409_), .C(_407_), .Y(_410_) );
OAI21X1 OAI21X1_30 ( .A(_384_), .B(_410_), .C(_406_), .Y(_381__0_) );
INVX1 INVX1_103 ( .A(_408_), .Y(_411_) );
XNOR2X1 XNOR2X1_15 ( .A(_21__29_), .B(_21__28_), .Y(_412_) );
OR2X2 OR2X2_16 ( .A(_412_), .B(_411_), .Y(_413_) );
INVX1 INVX1_104 ( .A(_405_), .Y(_414_) );
AOI22X1 AOI22X1_37 ( .A(_409_), .B(_412_), .C(D[29]), .D(_414_), .Y(_415_) );
AOI21X1 AOI21X1_54 ( .A(_415_), .B(_413_), .C(_384_), .Y(_381__1_) );
INVX1 INVX1_105 ( .A(_396_), .Y(_416_) );
NOR2X1 NOR2X1_60 ( .A(_400_), .B(_416_), .Y(_417_) );
AOI22X1 AOI22X1_38 ( .A(_414_), .B(D[30]), .C(_394_), .D(_417_), .Y(_418_) );
NAND2X1 NAND2X1_61 ( .A(_396_), .B(_401_), .Y(_419_) );
INVX1 INVX1_106 ( .A(_21__30_), .Y(_420_) );
XNOR2X1 XNOR2X1_16 ( .A(_388_), .B(_420_), .Y(_421_) );
AOI22X1 AOI22X1_39 ( .A(_419_), .B(_399_), .C(_409_), .D(_421_), .Y(_422_) );
AOI21X1 AOI21X1_55 ( .A(_422_), .B(_418_), .C(_384_), .Y(_381__2_) );
NAND3X1 NAND3X1_40 ( .A(_386_), .B(D[31]), .C(_404_), .Y(_423_) );
OAI21X1 OAI21X1_31 ( .A(_387_), .B(_390_), .C(_423_), .Y(_424_) );
AOI21X1 AOI21X1_56 ( .A(_399_), .B(_402_), .C(_424_), .Y(_425_) );
NAND2X1 NAND2X1_62 ( .A(_21__31_), .B(_416_), .Y(_426_) );
AOI21X1 AOI21X1_57 ( .A(_395_), .B(_396_), .C(_393_), .Y(_427_) );
NOR3X1 NOR3X1_9 ( .A(_21__30_), .B(_21__29_), .C(_21__28_), .Y(_428_) );
OAI22X1 OAI22X1_8 ( .A(_428_), .B(_387_), .C(_398_), .D(_400_), .Y(_429_) );
AOI22X1 AOI22X1_40 ( .A(_427_), .B(_426_), .C(_21__31_), .D(_429_), .Y(_430_) );
AOI21X1 AOI21X1_58 ( .A(_425_), .B(_430_), .C(_384_), .Y(_381__3_) );
DFFPOSX1 DFFPOSX1_43 ( .CLK(clk7), .D(_381__0_), .Q(_21__28_) );
DFFPOSX1 DFFPOSX1_44 ( .CLK(clk7), .D(_381__1_), .Q(_21__29_) );
DFFPOSX1 DFFPOSX1_45 ( .CLK(clk7), .D(_381__2_), .Q(_21__30_) );
DFFPOSX1 DFFPOSX1_46 ( .CLK(clk7), .D(_381__3_), .Q(_21__31_) );
DFFPOSX1 DFFPOSX1_47 ( .CLK(clk7), .D(_380_), .Q(LOAD7) );
DFFPOSX1 DFFPOSX1_48 ( .CLK(clk7), .D(_382_), .Q(RCO7) );
FILL FILL_0_0_0 ( );
FILL FILL_0_0_1 ( );
FILL FILL_0_0_2 ( );
FILL FILL_0_1_0 ( );
FILL FILL_0_1_1 ( );
FILL FILL_0_1_2 ( );
FILL FILL_1_1 ( );
FILL FILL_1_2 ( );
FILL FILL_1_0_0 ( );
FILL FILL_1_0_1 ( );
FILL FILL_1_0_2 ( );
FILL FILL_1_1_0 ( );
FILL FILL_1_1_1 ( );
FILL FILL_1_1_2 ( );
FILL FILL_2_1 ( );
FILL FILL_2_2 ( );
FILL FILL_2_0_0 ( );
FILL FILL_2_0_1 ( );
FILL FILL_2_0_2 ( );
FILL FILL_2_1_0 ( );
FILL FILL_2_1_1 ( );
FILL FILL_2_1_2 ( );
FILL FILL_3_0_0 ( );
FILL FILL_3_0_1 ( );
FILL FILL_3_0_2 ( );
FILL FILL_3_1_0 ( );
FILL FILL_3_1_1 ( );
FILL FILL_3_1_2 ( );
FILL FILL_4_0_0 ( );
FILL FILL_4_0_1 ( );
FILL FILL_4_0_2 ( );
FILL FILL_4_1_0 ( );
FILL FILL_4_1_1 ( );
FILL FILL_4_1_2 ( );
FILL FILL_5_0_0 ( );
FILL FILL_5_0_1 ( );
FILL FILL_5_0_2 ( );
FILL FILL_5_1_0 ( );
FILL FILL_5_1_1 ( );
FILL FILL_5_1_2 ( );
FILL FILL_6_0_0 ( );
FILL FILL_6_0_1 ( );
FILL FILL_6_0_2 ( );
FILL FILL_6_1_0 ( );
FILL FILL_6_1_1 ( );
FILL FILL_6_1_2 ( );
FILL FILL_7_1 ( );
FILL FILL_7_0_0 ( );
FILL FILL_7_0_1 ( );
FILL FILL_7_0_2 ( );
FILL FILL_7_1_0 ( );
FILL FILL_7_1_1 ( );
FILL FILL_7_1_2 ( );
FILL FILL_8_0_0 ( );
FILL FILL_8_0_1 ( );
FILL FILL_8_0_2 ( );
FILL FILL_8_1_0 ( );
FILL FILL_8_1_1 ( );
FILL FILL_8_1_2 ( );
FILL FILL_9_0_0 ( );
FILL FILL_9_0_1 ( );
FILL FILL_9_0_2 ( );
FILL FILL_9_1_0 ( );
FILL FILL_9_1_1 ( );
FILL FILL_9_1_2 ( );
FILL FILL_10_1 ( );
FILL FILL_10_2 ( );
FILL FILL_10_0_0 ( );
FILL FILL_10_0_1 ( );
FILL FILL_10_0_2 ( );
FILL FILL_10_1_0 ( );
FILL FILL_10_1_1 ( );
FILL FILL_10_1_2 ( );
FILL FILL_11_0_0 ( );
FILL FILL_11_0_1 ( );
FILL FILL_11_0_2 ( );
FILL FILL_11_1_0 ( );
FILL FILL_11_1_1 ( );
FILL FILL_11_1_2 ( );
endmodule
