/* Verilog module written by DEF2Verilog (qflow) */
module counter (
    input CLK,
    input CLK_HALF,
    input [3:0] D,
    input ENABLE,
    output LOAD,
    input [2:0] MODO,
    output [3:0] Q,
    output RCO,
    input RESET
);

wire [3:0] D ;
wire [3:0] Q ;
wire _19_ ;
wire _16_ ;
wire _13_ ;
wire _51_ ;
wire _7_ ;
wire _48_ ;
wire _10_ ;
wire _4_ ;
wire _45_ ;
wire RESET ;
wire [3:0] _1_ ;
wire _42_ ;
wire _39_ ;
wire _36_ ;
wire _33_ ;
wire _30_ ;
wire _27_ ;
wire [2:0] MODO ;
wire _24_ ;
wire _21_ ;
wire _18_ ;
wire ENABLE ;
wire LOAD ;
wire _15_ ;
wire _53_ ;
wire _9_ ;
wire _12_ ;
wire _50_ ;
wire _6_ ;
wire RCO ;
wire _47_ ;
wire _3_ ;
wire _44_ ;
wire _0_ ;
wire _41_ ;
wire _38_ ;
wire _35_ ;
wire CLK_HALF ;
wire _32_ ;
wire _29_ ;
wire _26_ ;
wire CLK ;
wire _23_ ;
wire _20_ ;
wire _17_ ;
wire _14_ ;
wire [3:0] _52_ ;
wire _8_ ;
wire _49_ ;
wire _11_ ;
wire _5_ ;
wire _46_ ;
wire gnd = 1'b0 ;
wire _2_ ;
wire _43_ ;
wire _40_ ;
wire _37_ ;
wire _34_ ;
wire _31_ ;
wire vdd = 1'b1 ;
wire _28_ ;
wire _25_ ;
wire _22_ ;

NOR2X1 _60_ (
    .A(_52_[3]),
    .B(_52_[2]),
    .Y(_9_)
);

INVX2 _57_ (
    .A(MODO[2]),
    .Y(_6_)
);

INVX1 _95_ (
    .A(_52_[2]),
    .Y(_40_)
);

FILL SFILL9840x4100 (
);

INVX1 _54_ (
    .A(RESET),
    .Y(_3_)
);

NOR2X1 _92_ (
    .A(_20_),
    .B(_36_),
    .Y(_37_)
);

AOI22X1 _89_ (
    .A(_29_),
    .B(_32_),
    .C(D[1]),
    .D(_34_),
    .Y(_35_)
);

FILL SFILL3600x4100 (
);

FILL FILL11120x6100 (
);

XNOR2X1 _86_ (
    .A(_52_[1]),
    .B(_52_[0]),
    .Y(_32_)
);

DFFPOSX1 _118_ (
    .Q(_51_),
    .CLK(CLK),
    .D(_0_)
);

FILL SFILL9360x6100 (
);

OAI21X1 _83_ (
    .A(_28_),
    .B(_29_),
    .C(_27_),
    .Y(_30_)
);

DFFPOSX1 _115_ (
    .Q(_52_[1]),
    .CLK(CLK),
    .D(_1_[1])
);

INVX1 _80_ (
    .A(_52_[0]),
    .Y(_27_)
);

BUFX2 _112_ (
    .A(_52_[3]),
    .Y(Q[3])
);

NAND2X1 _77_ (
    .A(_6_),
    .B(_24_),
    .Y(_25_)
);

BUFX2 _109_ (
    .A(_52_[0]),
    .Y(Q[0])
);

FILL SFILL9360x4100 (
);

AOI22X1 _74_ (
    .A(_14_),
    .B(_17_),
    .C(_19_),
    .D(_22_),
    .Y(_23_)
);

AOI22X1 _106_ (
    .A(_47_),
    .B(_46_),
    .C(_52_[3]),
    .D(_49_),
    .Y(_50_)
);

FILL SFILL3920x100 (
);

FILL SFILL3920x6100 (
);

AOI21X1 _71_ (
    .A(_52_[1]),
    .B(_52_[0]),
    .C(_52_[2]),
    .Y(_20_)
);

AOI21X1 _103_ (
    .A(_15_),
    .B(_16_),
    .C(_13_),
    .Y(_47_)
);

NOR2X1 _68_ (
    .A(_15_),
    .B(_16_),
    .Y(_17_)
);

FILL SFILL4240x2100 (
);

OAI21X1 _100_ (
    .A(_7_),
    .B(_10_),
    .C(_43_),
    .Y(_44_)
);

FILL SFILL9360x2100 (
);

INVX1 _65_ (
    .A(_13_),
    .Y(_14_)
);

FILL SFILL4240x100 (
);

FILL SFILL3920x4100 (
);

OR2X2 _62_ (
    .A(_10_),
    .B(_7_),
    .Y(_11_)
);

NOR2X1 _59_ (
    .A(_52_[1]),
    .B(_52_[0]),
    .Y(_8_)
);

AOI22X1 _97_ (
    .A(_39_),
    .B(_19_),
    .C(_29_),
    .D(_41_),
    .Y(_42_)
);

FILL SFILL9040x2100 (
);

FILL SFILL9680x6100 (
);

INVX1 _56_ (
    .A(MODO[1]),
    .Y(_5_)
);

NAND2X1 _94_ (
    .A(_16_),
    .B(_21_),
    .Y(_39_)
);

FILL SFILL4080x100 (
);

FILL SFILL3920x2100 (
);

FILL SFILL3440x6100 (
);

INVX1 _91_ (
    .A(_16_),
    .Y(_36_)
);

INVX1 _88_ (
    .A(_25_),
    .Y(_34_)
);

FILL SFILL9680x4100 (
);

INVX1 _85_ (
    .A(_28_),
    .Y(_31_)
);

DFFPOSX1 _117_ (
    .Q(_52_[3]),
    .CLK(CLK),
    .D(_1_[3])
);

INVX1 _82_ (
    .A(_7_),
    .Y(_29_)
);

DFFPOSX1 _114_ (
    .Q(_52_[0]),
    .CLK(CLK),
    .D(_1_[0])
);

NAND2X1 _79_ (
    .A(D[0]),
    .B(_0_),
    .Y(_26_)
);

FILL SFILL9200x6100 (
);

BUFX2 _111_ (
    .A(_52_[2]),
    .Y(Q[2])
);

AND2X2 _76_ (
    .A(MODO[0]),
    .B(MODO[1]),
    .Y(_24_)
);

BUFX2 _108_ (
    .A(_51_),
    .Y(LOAD)
);

NOR2X1 _73_ (
    .A(_52_[3]),
    .B(_21_),
    .Y(_22_)
);

OAI22X1 _105_ (
    .A(_48_),
    .B(_7_),
    .C(_18_),
    .D(_20_),
    .Y(_49_)
);

INVX1 _70_ (
    .A(_18_),
    .Y(_19_)
);

FILL SFILL4080x4100 (
);

NAND2X1 _102_ (
    .A(_52_[3]),
    .B(_36_),
    .Y(_46_)
);

NAND3X1 _67_ (
    .A(_52_[2]),
    .B(_52_[1]),
    .C(_52_[0]),
    .Y(_16_)
);

FILL SFILL3760x100 (
);

FILL SFILL3760x6100 (
);

NAND3X1 _64_ (
    .A(_12_),
    .B(_5_),
    .C(_6_),
    .Y(_13_)
);

NAND3X1 _99_ (
    .A(_6_),
    .B(D[3]),
    .C(_24_),
    .Y(_43_)
);

FILL SFILL9200x2100 (
);

NAND2X1 _61_ (
    .A(_8_),
    .B(_9_),
    .Y(_10_)
);

FILL SFILL4080x2100 (
);

NAND3X1 _58_ (
    .A(MODO[0]),
    .B(_5_),
    .C(_6_),
    .Y(_7_)
);

XNOR2X1 _96_ (
    .A(_8_),
    .B(_40_),
    .Y(_41_)
);

FILL SFILL3760x4100 (
);

NAND2X1 _55_ (
    .A(ENABLE),
    .B(_3_),
    .Y(_4_)
);

AOI22X1 _93_ (
    .A(_34_),
    .B(D[2]),
    .C(_14_),
    .D(_37_),
    .Y(_38_)
);

FILL FILL11280x6100 (
);

FILL SFILL9520x6100 (
);

AOI21X1 _90_ (
    .A(_35_),
    .B(_33_),
    .C(_4_),
    .Y(_1_[1])
);

OR2X2 _87_ (
    .A(_32_),
    .B(_31_),
    .Y(_33_)
);

FILL SFILL9200x100 (
);

DFFPOSX1 _119_ (
    .Q(_53_),
    .CLK(CLK),
    .D(_2_)
);

OAI21X1 _84_ (
    .A(_4_),
    .B(_30_),
    .C(_26_),
    .Y(_1_[0])
);

FILL FILL11280x4100 (
);

FILL SFILL8880x2100 (
);

DFFPOSX1 _116_ (
    .Q(_52_[2]),
    .CLK(CLK),
    .D(_1_[2])
);

FILL SFILL9520x4100 (
);

NOR2X1 _81_ (
    .A(MODO[0]),
    .B(MODO[2]),
    .Y(_28_)
);

BUFX2 _113_ (
    .A(_53_),
    .Y(RCO)
);

NOR2X1 _78_ (
    .A(_4_),
    .B(_25_),
    .Y(_0_)
);

FILL SFILL9040x100 (
);

BUFX2 _110_ (
    .A(_52_[1]),
    .Y(Q[1])
);

AOI21X1 _75_ (
    .A(_23_),
    .B(_11_),
    .C(_4_),
    .Y(_2_)
);

FILL SFILL9520x100 (
);

AOI21X1 _107_ (
    .A(_45_),
    .B(_50_),
    .C(_4_),
    .Y(_1_[3])
);

FILL SFILL4400x2100 (
);

INVX1 _72_ (
    .A(_20_),
    .Y(_21_)
);

NOR3X1 _104_ (
    .A(_52_[2]),
    .B(_52_[1]),
    .C(_52_[0]),
    .Y(_48_)
);

NAND3X1 _69_ (
    .A(MODO[1]),
    .B(_12_),
    .C(_6_),
    .Y(_18_)
);

AOI21X1 _101_ (
    .A(_19_),
    .B(_22_),
    .C(_44_),
    .Y(_45_)
);

INVX1 _66_ (
    .A(_52_[3]),
    .Y(_15_)
);

FILL SFILL9360x100 (
);

INVX1 _63_ (
    .A(MODO[0]),
    .Y(_12_)
);

AOI21X1 _98_ (
    .A(_42_),
    .B(_38_),
    .C(_4_),
    .Y(_1_[2])
);

FILL SFILL3600x6100 (
);

endmodule
