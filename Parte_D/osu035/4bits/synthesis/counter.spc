*SPICE netlist created from BLIF module counter by blif2BSpice
.include /usr/share/qflow/tech/osu035/osu035_stdcells.sp
.subckt counter vdd gnd CLK ENABLE RESET CLK_HALF MODO[0] MODO[1] MODO[2] D[0] D[1] D[2] D[3] Q[0] Q[1] Q[2] Q[3] RCO LOAD 
XOAI21X1_1 gnd vdd _7_ _10_ _44_ _43_ OAI21X1
XAOI21X1_1 gnd vdd _19_ _22_ _45_ _44_ AOI21X1
XNAND2X1_1 vdd _46_ gnd _52_[3] _36_ NAND2X1
XAOI21X1_2 gnd vdd _15_ _16_ _47_ _13_ AOI21X1
XNOR3X1_1 vdd gnd _52_[1] _52_[0] _52_[2] _48_ NOR3X1
XOAI22X1_1 gnd vdd _20_ _18_ _48_ _7_ _49_ OAI22X1
XAOI22X1_1 gnd vdd _52_[3] _49_ _50_ _47_ _46_ AOI22X1
XAOI21X1_3 gnd vdd _45_ _50_ _1_[3] _4_ AOI21X1
XBUFX2_1 vdd gnd _51_ LOAD BUFX2
XBUFX2_2 vdd gnd _52_[0] Q[0] BUFX2
XBUFX2_3 vdd gnd _52_[1] Q[1] BUFX2
XBUFX2_4 vdd gnd _52_[2] Q[2] BUFX2
XBUFX2_5 vdd gnd _52_[3] Q[3] BUFX2
XBUFX2_6 vdd gnd _53_ RCO BUFX2
XDFFPOSX1_1 vdd _1_[0] gnd _52_[0] CLK DFFPOSX1
XDFFPOSX1_2 vdd _1_[1] gnd _52_[1] CLK DFFPOSX1
XDFFPOSX1_3 vdd _1_[2] gnd _52_[2] CLK DFFPOSX1
XDFFPOSX1_4 vdd _1_[3] gnd _52_[3] CLK DFFPOSX1
XDFFPOSX1_5 vdd _0_ gnd _51_ CLK DFFPOSX1
XDFFPOSX1_6 vdd _2_ gnd _53_ CLK DFFPOSX1
XINVX1_1 RESET _3_ vdd gnd INVX1
XNAND2X1_2 vdd _4_ gnd ENABLE _3_ NAND2X1
XINVX1_2 MODO[1] _5_ vdd gnd INVX1
XINVX2_1 vdd gnd _6_ MODO[2] INVX2
XNAND3X1_1 _5_ vdd gnd MODO[0] _6_ _7_ NAND3X1
XNOR2X1_1 vdd _52_[0] gnd _8_ _52_[1] NOR2X1
XNOR2X1_2 vdd _52_[2] gnd _9_ _52_[3] NOR2X1
XNAND2X1_3 vdd _10_ gnd _8_ _9_ NAND2X1
XOR2X2_1 _11_ _7_ vdd gnd _10_ OR2X2
XINVX1_3 MODO[0] _12_ vdd gnd INVX1
XNAND3X1_2 _5_ vdd gnd _12_ _6_ _13_ NAND3X1
XINVX1_4 _13_ _14_ vdd gnd INVX1
XINVX1_5 _52_[3] _15_ vdd gnd INVX1
XNAND3X1_3 _52_[1] vdd gnd _52_[2] _52_[0] _16_ NAND3X1
XNOR2X1_3 vdd _16_ gnd _17_ _15_ NOR2X1
XNAND3X1_4 _12_ vdd gnd MODO[1] _6_ _18_ NAND3X1
XINVX1_6 _18_ _19_ vdd gnd INVX1
XAOI21X1_4 gnd vdd _52_[1] _52_[0] _20_ _52_[2] AOI21X1
XINVX1_7 _20_ _21_ vdd gnd INVX1
XNOR2X1_4 vdd _21_ gnd _22_ _52_[3] NOR2X1
XAOI22X1_2 gnd vdd _19_ _22_ _23_ _14_ _17_ AOI22X1
XAOI21X1_5 gnd vdd _23_ _11_ _2_ _4_ AOI21X1
XAND2X2_1 vdd gnd MODO[0] MODO[1] _24_ AND2X2
XNAND2X1_4 vdd _25_ gnd _6_ _24_ NAND2X1
XNOR2X1_5 vdd _25_ gnd _0_ _4_ NOR2X1
XNAND2X1_5 vdd _26_ gnd D[0] _0_ NAND2X1
XINVX1_8 _52_[0] _27_ vdd gnd INVX1
XNOR2X1_6 vdd MODO[2] gnd _28_ MODO[0] NOR2X1
XINVX1_9 _7_ _29_ vdd gnd INVX1
XOAI21X1_2 gnd vdd _28_ _29_ _30_ _27_ OAI21X1
XOAI21X1_3 gnd vdd _4_ _30_ _1_[0] _26_ OAI21X1
XINVX1_10 _28_ _31_ vdd gnd INVX1
XXNOR2X1_1 _52_[1] _52_[0] gnd vdd _32_ XNOR2X1
XOR2X2_2 _33_ _31_ vdd gnd _32_ OR2X2
XINVX1_11 _25_ _34_ vdd gnd INVX1
XAOI22X1_3 gnd vdd D[1] _34_ _35_ _29_ _32_ AOI22X1
XAOI21X1_6 gnd vdd _35_ _33_ _1_[1] _4_ AOI21X1
XINVX1_12 _16_ _36_ vdd gnd INVX1
XNOR2X1_7 vdd _36_ gnd _37_ _20_ NOR2X1
XAOI22X1_4 gnd vdd _14_ _37_ _38_ _34_ D[2] AOI22X1
XNAND2X1_6 vdd _39_ gnd _16_ _21_ NAND2X1
XINVX1_13 _52_[2] _40_ vdd gnd INVX1
XXNOR2X1_2 _8_ _40_ gnd vdd _41_ XNOR2X1
XAOI22X1_5 gnd vdd _29_ _41_ _42_ _39_ _19_ AOI22X1
XAOI21X1_7 gnd vdd _42_ _38_ _1_[2] _4_ AOI21X1
XNAND3X1_5 D[3] vdd gnd _6_ _24_ _43_ NAND3X1
.ends counter
 