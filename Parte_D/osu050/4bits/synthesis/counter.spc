*SPICE netlist created from BLIF module counter by blif2BSpice
.include /usr/share/qflow/tech/osu050/osu050_stdcells.sp
.subckt counter vdd gnd CLK ENABLE RESET CLK_HALF MODO[0] MODO[1] MODO[2] D[0] D[1] D[2] D[3] Q[0] Q[1] Q[2] Q[3] RCO LOAD 
XNOR3X1_1 vdd gnd _50_[1] _50_[0] _50_[2] _46_ NOR3X1
XOAI22X1_1 gnd vdd _46_ _7_ _18_ _35_ _47_ OAI22X1
XAOI22X1_1 gnd vdd _50_[3] _47_ _48_ _45_ _13_ AOI22X1
XAOI21X1_1 gnd vdd _44_ _48_ _1_[3] _4_ AOI21X1
XBUFX2_1 vdd gnd _49_ LOAD BUFX2
XBUFX2_2 vdd gnd _50_[0] Q[0] BUFX2
XBUFX2_3 vdd gnd _50_[1] Q[1] BUFX2
XBUFX2_4 vdd gnd _50_[2] Q[2] BUFX2
XBUFX2_5 vdd gnd _50_[3] Q[3] BUFX2
XBUFX2_6 vdd gnd _51_ RCO BUFX2
XDFFPOSX1_1 vdd _1_[0] gnd _50_[0] CLK DFFPOSX1
XDFFPOSX1_2 vdd _1_[1] gnd _50_[1] CLK DFFPOSX1
XDFFPOSX1_3 vdd _1_[2] gnd _50_[2] CLK DFFPOSX1
XDFFPOSX1_4 vdd _1_[3] gnd _50_[3] CLK DFFPOSX1
XDFFPOSX1_5 vdd _0_ gnd _49_ CLK DFFPOSX1
XDFFPOSX1_6 vdd _2_ gnd _51_ CLK DFFPOSX1
XINVX1_1 RESET _3_ vdd gnd INVX1
XNAND2X1_1 vdd _4_ gnd ENABLE _3_ NAND2X1
XINVX1_2 MODO[1] _5_ vdd gnd INVX1
XINVX1_3 MODO[2] _6_ vdd gnd INVX1
XNAND3X1_1 _5_ vdd gnd MODO[0] _6_ _7_ NAND3X1
XNOR2X1_1 vdd _50_[0] gnd _8_ _50_[1] NOR2X1
XNOR2X1_2 vdd _50_[2] gnd _9_ _50_[3] NOR2X1
XNAND2X1_2 vdd _10_ gnd _8_ _9_ NAND2X1
XOR2X2_1 _11_ _7_ vdd gnd _10_ OR2X2
XNOR2X1_3 vdd MODO[2] gnd _12_ MODO[0] NOR2X1
XAND2X2_1 vdd gnd _12_ _5_ _13_ AND2X2
XINVX1_4 _50_[3] _14_ vdd gnd INVX1
XNAND3X1_2 _50_[1] vdd gnd _50_[2] _50_[0] _15_ NAND3X1
XNOR2X1_4 vdd _15_ gnd _16_ _14_ NOR2X1
XINVX1_5 MODO[0] _17_ vdd gnd INVX1
XNAND3X1_3 _17_ vdd gnd MODO[1] _6_ _18_ NAND3X1
XINVX1_6 _50_[2] _19_ vdd gnd INVX1
XNAND2X1_3 vdd _20_ gnd _50_[1] _50_[0] NAND2X1
XNAND3X1_4 _19_ vdd gnd _14_ _20_ _21_ NAND3X1
XNOR2X1_5 vdd _21_ gnd _22_ _18_ NOR2X1
XAOI21X1_2 gnd vdd _13_ _16_ _23_ _22_ AOI21X1
XAOI21X1_3 gnd vdd _23_ _11_ _2_ _4_ AOI21X1
XAND2X2_2 vdd gnd MODO[0] MODO[1] _24_ AND2X2
XNAND2X1_4 vdd _25_ gnd _6_ _24_ NAND2X1
XNOR2X1_6 vdd _25_ gnd _0_ _4_ NOR2X1
XNAND2X1_5 vdd _26_ gnd D[0] _0_ NAND2X1
XINVX1_7 _50_[0] _27_ vdd gnd INVX1
XINVX1_8 _7_ _28_ vdd gnd INVX1
XOAI21X1_1 gnd vdd _12_ _28_ _29_ _27_ OAI21X1
XOAI21X1_2 gnd vdd _4_ _29_ _1_[0] _26_ OAI21X1
XINVX1_9 _8_ _30_ vdd gnd INVX1
XNAND3X1_5 _20_ vdd gnd _12_ _30_ _31_ NAND3X1
XINVX1_10 _25_ _32_ vdd gnd INVX1
XNAND2X1_6 vdd _33_ gnd _20_ _30_ NAND2X1
XAOI22X1_2 gnd vdd D[1] _32_ _34_ _33_ _28_ AOI22X1
XAOI21X1_4 gnd vdd _34_ _31_ _1_[1] _4_ AOI21X1
XAOI21X1_5 gnd vdd _50_[1] _50_[0] _35_ _50_[2] AOI21X1
XINVX1_11 _35_ _36_ vdd gnd INVX1
XAND2X2_3 vdd gnd _36_ _15_ _37_ AND2X2
XAOI22X1_3 gnd vdd _37_ _13_ _38_ D[2] _32_ AOI22X1
XXOR2X1_1 _39_ vdd _50_[2] _8_ gnd XOR2X1
XAOI21X1_6 gnd vdd _36_ _15_ _40_ _18_ AOI21X1
XAOI21X1_7 gnd vdd _39_ _28_ _41_ _40_ AOI21X1
XAOI21X1_8 gnd vdd _41_ _38_ _1_[2] _4_ AOI21X1
XNAND3X1_6 D[3] vdd gnd _6_ _24_ _42_ NAND3X1
XOAI21X1_3 gnd vdd _7_ _10_ _43_ _42_ OAI21X1
XNOR2X1_7 vdd _43_ gnd _44_ _22_ NOR2X1
XXNOR2X1_1 _15_ _50_[3] gnd vdd _45_ XNOR2X1
XFILL_0_0_0 vdd gnd FILL
XFILL_0_0_1 vdd gnd FILL
XFILL_0_0_2 vdd gnd FILL
XFILL_0_1_0 vdd gnd FILL
XFILL_0_1_1 vdd gnd FILL
XFILL_0_1_2 vdd gnd FILL
XFILL_1_1 vdd gnd FILL
XFILL_1_0_0 vdd gnd FILL
XFILL_1_0_1 vdd gnd FILL
XFILL_1_0_2 vdd gnd FILL
XFILL_1_1_0 vdd gnd FILL
XFILL_1_1_1 vdd gnd FILL
XFILL_1_1_2 vdd gnd FILL
XFILL_2_1 vdd gnd FILL
XFILL_2_2 vdd gnd FILL
XFILL_2_0_0 vdd gnd FILL
XFILL_2_0_1 vdd gnd FILL
XFILL_2_0_2 vdd gnd FILL
XFILL_2_1_0 vdd gnd FILL
XFILL_2_1_1 vdd gnd FILL
XFILL_2_1_2 vdd gnd FILL
XFILL_3_0_0 vdd gnd FILL
XFILL_3_0_1 vdd gnd FILL
XFILL_3_0_2 vdd gnd FILL
XFILL_3_1_0 vdd gnd FILL
XFILL_3_1_1 vdd gnd FILL
XFILL_3_1_2 vdd gnd FILL
.ends counter
 
