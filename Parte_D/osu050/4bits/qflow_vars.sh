#!/usr/bin/tcsh -f
#-------------------------------------------
# qflow variables for project /home/emma_rivel/EIE/Cursos/Microelectronica/proyecto_final/Parte_D/osu050/4bits
#-------------------------------------------

set projectpath=/home/emma_rivel/EIE/Cursos/Microelectronica/proyecto_final/Parte_D/osu050/4bits
set techdir=/usr/share/qflow/tech/osu050
set sourcedir=/home/emma_rivel/EIE/Cursos/Microelectronica/proyecto_final/Parte_D/osu050/4bits/source
set synthdir=/home/emma_rivel/EIE/Cursos/Microelectronica/proyecto_final/Parte_D/osu050/4bits/synthesis
set layoutdir=/home/emma_rivel/EIE/Cursos/Microelectronica/proyecto_final/Parte_D/osu050/4bits/layout
set techname=osu050
set scriptdir=/usr/lib/qflow/scripts
set bindir=/usr/lib/qflow/bin
set logdir=/home/emma_rivel/EIE/Cursos/Microelectronica/proyecto_final/Parte_D/osu050/4bits/log
#-------------------------------------------

