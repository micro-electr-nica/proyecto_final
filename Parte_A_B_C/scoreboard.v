`timescale 1ns / 1ps

module scoreboard_32(
    input CLK,
    input ENABLE,
    input RESET,
    input [2:0] MODO,
    input [31:0] D,
    output reg [31:0] Q_sb,
    output reg RCO_sb,
    output reg LOAD_sb
);

    always @(posedge CLK) begin
        if (ENABLE) begin
            if (MODO == 3'b000) begin
                Q_sb <= Q_sb+1;
                LOAD_sb <= 0;
                if (Q_sb == 32'hFFFFFFFF) begin
                    RCO_sb <= 1;
                end
                else begin
                    RCO_sb <= 0;
                end
            end
            else if(MODO == 3'b001) begin
                Q_sb <= Q_sb-1;
                LOAD_sb <= 0;
                if (Q_sb == 32'h00000000) begin
                    RCO_sb <= 1;
                end
                else begin
                    RCO_sb <= 0;
                end
            end
            else if(MODO == 3'b010) begin
                Q_sb <= Q_sb-3;
                LOAD_sb <= 0;
                if ((Q_sb == 32'h00000002) | (Q_sb == 32'h00000001) | (Q_sb == 32'h00000000)) begin
                    RCO_sb <= 1;
                end
                else begin
                    RCO_sb <= 0;
                end
            end
            else if(MODO == 3'b011) begin
                Q_sb <= D;
                RCO_sb <= 0;
                LOAD_sb <= 1;
            end
            else if(MODO == 3'b100) begin
                Q_sb <= 0;
                RCO_sb <= 0;
                LOAD_sb <= 0;
            end
        end   
        else begin
            Q_sb <= 0;
            RCO_sb <= 0;
            LOAD_sb <= 0;
        end
    end
endmodule