task driver_init;
  begin
    @(posedge CLK) begin
      RESET = 1;
      ENABLE = 0;
    end
    @(posedge CLK) begin
      RESET = 0;
      ENABLE = 1;
    end
    @(posedge CLK) begin
      RESET = 0;
      ENABLE = 1;
    end
  end
endtask

task driver_request;
  input integer iteration;
  repeat (iteration) begin  
    @(posedge CLK) begin
        ENABLE = 1;
        MODO = 'b0;
        D_32 = 'b0;
        D_4 = 'b0;
        @(posedge CLK);
        @(posedge CLK);
        @(posedge CLK);
        @(posedge CLK);
        @(posedge CLK);
        MODO = 'b01;
        @(posedge CLK);
        @(posedge CLK);
        @(posedge CLK);
        @(posedge CLK);
        @(posedge CLK);
        MODO = 'b11;
        D_32 = 'hF;
        D_4 = 'hF;
        @(posedge CLK);
        MODO = 'b10;
        @(posedge CLK);
        @(posedge CLK);
        @(posedge CLK);
        @(posedge CLK);
        @(posedge CLK);
    end
  end
endtask

task rand_counter;
  input integer iteration;
  repeat (iteration) begin  
    @(posedge CLK) begin
        D_4[0] = $random;
        D_4[1] = $random;
        D_4[2] = $random;
        D_4[3] = $random;
        MODO[0] = $random;
        D_4 = $random;
        MODO[1] = $random;
    end
  end
endtask

task rco_logic_test;
  input integer iteration;
  repeat (iteration) begin  
    D_32 = $random;
    D_4 = $random;
    @(posedge CLK) begin
    // TEST: MAX + 1
    MODO <= 'b11;
    @(posedge CLK);
    D_32 <= 'hFFFFFFFF;
    D_4 <= 'hF;
    @(posedge CLK);
    @(posedge CLK);
    MODO <= 'b00;
    @(posedge CLK);
    @(posedge CLK);
    // TEST: MIN - 1
    MODO <= 'b11;
    @(posedge CLK);
    D_32 <= 'h0;
    D_4 <= 'h0;
    @(posedge CLK);
    @(posedge CLK);
    MODO <= 'b01;
    @(posedge CLK);
    @(posedge CLK);
    @(posedge CLK);
    @(posedge CLK);
    // TEST: (MIN+N) - 3, N=2
    MODO <= 'b11;
    @(posedge CLK);
    D_32 <= 'b10;
    D_4 <= 'b10;
    @(posedge CLK);
    @(posedge CLK);
    MODO <= 'b10;
    @(posedge CLK);
    @(posedge CLK);
    @(posedge CLK);
    @(posedge CLK);
    end
  end
endtask

task RESET_breaker;
  begin
    @(posedge CLK) begin
      RESET = 'b0;
      ENABLE = 'b0;
    end
    @(posedge CLK);
    @(posedge CLK);
    @(posedge CLK);
    @(posedge CLK);
    RESET = 'b1;
    @(posedge CLK);
    @(posedge CLK);
    @(posedge CLK);
    @(posedge CLK) begin
      RESET = 'b0;
      ENABLE = 'b1;
    end
    @(posedge CLK);
    @(posedge CLK);
  end
endtask
