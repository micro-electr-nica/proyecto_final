`timescale 1ns / 1ps

module counter #(
    parameter M0 = 3'b00, 
    parameter M1 = 3'b01, 
    parameter M2 = 3'b10, 
    parameter M3 = 3'b11
    )(
    input CLK, ENABLE, RESET, CLK_HALF,
    input [2:0] MODO,
    input [3:0] D,
    output reg [3:0] Q,
    output reg RCO, LOAD); 
    always @(posedge CLK)begin
        if ((!ENABLE && !RESET) || RESET || !ENABLE) begin
            Q <= 'b0;
            RCO <= 'b0;
            LOAD <= 'b0;
        end
        else begin
            if (ENABLE) begin
                case(MODO)
                    M0: begin
                        Q <= Q+1;
                        LOAD <= 'b0;
                        if (Q == 4'hF) begin
                            //@(posedge CLK_HALF) RCO <= 1;
                            RCO <= 1;
                        end else begin
                            RCO <= 0;
                        end
                    end
                    M1: begin
                        Q <= Q-1;
                        LOAD <= 'b0;
                        if (Q == 4'h0) begin
                            //@(posedge CLK_HALF) RCO <= 1;
                            RCO <= 1;
                        end else begin
                            RCO <= 'b0;
                        end
                    end
                    M2: begin
                        Q <= Q-3;
                        LOAD <= 'b0;
                        if (Q == 4'h2 | Q == 4'h0 | Q == 4'h1) begin
                            //@(posedge CLK_HALF) RCO <= 'b1;
                            RCO <= 1;
                        end else begin
                            RCO <= 'b0;
                        end
                    end
                    M3: begin
                        Q <= D;
                        RCO <= 'b0;
                        LOAD <= 'b1;
                    end
                    default: begin
                        Q <= 'b0;
                        RCO <= 'b0;
                        LOAD <= 'b0;
                    end
                endcase
            end
        end
    end
endmodule

module counter_32(
    input CLK, ENABLE, RESET, CLK_HALF,
    input [2:0] MODO,
    input [31:0] D,
    output [31:0] Q,
    output RCO,
    output LOAD); 
    wire RCO0, RCO1, RCO2, RCO3, RCO4, RCO5, RCO6, RCO7;
    wire LOAD0, LOAD1, LOAD2, LOAD3, LOAD4, LOAD5, LOAD6, LOAD7; 
    wire [3:0] Q0, Q1, Q2, Q3, Q4, Q5, Q6, Q7;
    wire clk1, clk2, clk3, clk4, clk5, clk6, clk7;
    wire [2:0] MODO0, MODO1, MODO2, MODO3, MODO4, MODO5, MODO6, MODO7;

	assign MODO0 = (MODO == 3'b000 || MODO == 3'b001 || MODO == 3'b010 || MODO == 3'b011 || MODO == 3'b100) ? MODO : 3'b100;

	assign clk1 = (MODO0 == 3'b100 || MODO0 == 3'b011) ? CLK  : RCO0;
	assign clk2 = (MODO0 == 3'b100 || MODO0 == 3'b011) ? CLK  : RCO1;
	assign clk3 = (MODO0 == 3'b100 || MODO0 == 3'b011) ? CLK  : RCO2;
	assign clk4 = (MODO0 == 3'b100 || MODO0 == 3'b011) ? CLK  : RCO3;
	assign clk5 = (MODO0 == 3'b100 || MODO0 == 3'b011) ? CLK  : RCO4;
	assign clk6 = (MODO0 == 3'b100 || MODO0 == 3'b011) ? CLK  : RCO5;
	assign clk7 = (MODO0 == 3'b100 || MODO0 == 3'b011) ? CLK  : RCO6;

	assign MODO1 = (MODO0 == 3'b010) ? 3'b001 : MODO0;
	assign MODO2 = (MODO0 == 3'b010) ? 3'b001 : MODO0;
	assign MODO3 = (MODO0 == 3'b010) ? 3'b001 : MODO0;
	assign MODO4 = (MODO0 == 3'b010) ? 3'b001 : MODO0;
	assign MODO5 = (MODO0 == 3'b010) ? 3'b001 : MODO0;
	assign MODO6 = (MODO0 == 3'b010) ? 3'b001 : MODO0;
	assign MODO7 = (MODO0 == 3'b010) ? 3'b001 : MODO0;


    assign LOAD = (LOAD0 && LOAD1 && LOAD2 && LOAD3 && LOAD4 && LOAD5 & LOAD6 & LOAD7);

	assign RCO = RCO2 && RCO3 && RCO4 && RCO5 && RCO6 && RCO7;
    
    assign Q[3:0] = Q0;
    assign Q[7:4] = Q1;
    assign Q[11:8] = Q2;
    assign Q[15:12] = Q3;
    assign Q[19:16] = Q4;
    assign Q[23:20] = Q5;
    assign Q[27:24] = Q6;
    assign Q[31:28] = Q7;

    counter contador0(
    /*AUTOINST*/    
	       // Outputs
	       .Q		(Q0),
	       .RCO		(RCO0),
           .LOAD    (LOAD0),
	       // Inputs
	       .CLK		(CLK),
           .CLK_HALF (CLK_HALF),
	       .ENABLE		(ENABLE),
           .RESET       (RESET),
	       .MODO		(MODO0),
	       .D		(D[3:0]));

    counter contador1(
       /*AUTOINST*/
		       // Outputs
		       .Q		(Q1),
		       .RCO		(RCO1),
               .LOAD    (LOAD1),
		       // Inputs
		       .CLK		(clk1),
               .CLK_HALF (CLK_HALF),
		       .ENABLE		(ENABLE),
               .RESET       (RESET),
		       .MODO		(MODO1),
		       .D		(D[7:4]));

    counter contador2(
        /*AUTOINST*/
		       // Outputs
		       .Q		(Q2),
		       .RCO		(RCO2),
               .LOAD    (LOAD2),
		       // Inputs
		       .CLK		(clk2),
               .CLK_HALF (CLK_HALF),
		       .ENABLE		(ENABLE),
               .RESET       (RESET),
		       .MODO		(MODO2),
		       .D		(D[11:8]));

    counter contador3(
        /*AUTOINST*/
		       // Outputs
		       .Q		(Q3),
		       .RCO		(RCO3),
               .LOAD    (LOAD3),
		       // Inputs
		       .CLK		(clk3),
               .CLK_HALF (CLK_HALF),
		       .ENABLE		(ENABLE),
               .RESET       (RESET),
		       .MODO		(MODO3),
		       .D		(D[15:12]));

    counter contador4(
        /*AUTOINST*/
		       // Outputs
		       .Q		(Q4),
		       .RCO		(RCO4),
               .LOAD    (LOAD4),
		       // Inputs
		       .CLK		(clk4),
               .CLK_HALF (CLK_HALF),
		       .ENABLE		(ENABLE),
               .RESET       (RESET),
		       .MODO		(MODO4),
		       .D		(D[19:16]));

    counter contador5(
        /*AUTOINST*/
		       // Outputs
		       .Q		(Q5),
		       .RCO		(RCO5),
               .LOAD    (LOAD5),
		       // Inputs
		       .CLK		(clk5),
               .CLK_HALF (CLK_HALF),
		       .ENABLE		(ENABLE),
               .RESET       (RESET),
		       .MODO		(MODO5),
		       .D		(D[23:20]));
    
    counter contador6(
        /*AUTOINST*/
		       // Outputs
		       .Q		(Q6),
		       .RCO		(RCO6),
               .LOAD    (LOAD6),
		       // Inputs
		       .CLK		(clk6),
               .CLK_HALF (CLK_HALF),
		       .ENABLE		(ENABLE),
               .RESET       (RESET),
		       .MODO		(MODO6),
		       .D		(D[27:24]));

    counter contador7(
        /*AUTOINST*/
		       // Outputs
		       .Q		(Q7),
		       .RCO		(RCO7),
               .LOAD    (LOAD7),
		       // Inputs
		       .CLK		(clk7),
               .CLK_HALF (CLK_HALF),
		       .ENABLE		(ENABLE),
               .RESET       (RESET),
		       .MODO		(MODO7),
		       .D		(D[31:28]));
endmodule