`timescale 1ns / 1ps
`include "scoreboard.v"
`include "scoreboard_4.v"

module contador_tb #(
    parameter ITERATIONS = 3
    )(
    output reg CLK, ENABLE, CLK_HALF, RESET,
    output reg [2:0] MODO,
    output reg [3:0] D_4,
    output reg [31:0] D_32,
    input RCO_32, RCO_4, LOAD_32, LOAD_4,
    input [31:0] Q_32,
    input [3:0] Q_4);

    `include "driver.v"
    `include "checker.v"
    integer log;
    wire RCO_sb_32, RCO_sb_4, LOAD_sb_32, LOAD_sb_4;
    wire [31:0] Q_sb_32;
    wire [3:0] Q_sb_4;

    initial begin

        $dumpfile("counters.vcd");
        $dumpvars(0);
        log = $fopen("tb.log");
        $fdisplay(log, "time=%5d, Simulation Start", $time);

        
        //Init reset
        $fdisplay(log, "time=%5d, Starting RESET", $time);
        driver_init();
        $fdisplay(log, "time=%5d, RESET Completed", $time);


        //Driver Request with know values 
        $fdisplay(log, "time=%5d, Starting Test: Driver Request", $time);
        fork
          driver_request(ITERATIONS);  
          checker(ITERATIONS);
        join
        $fdisplay(log, "time=%5d, Driver Request Completed", $time);

        //RCO logic test
        $fdisplay(log, "time=%5d, Starting Test: RCO Logic Test", $time);
        fork
          rco_logic_test(ITERATIONS);  
          checker(ITERATIONS);
        join
        $fdisplay(log, "time=%5d, RCO Logic Test Completed", $time);


        //End
        $fdisplay(log, "time=%5d, Simulation Completed", $time);
        $fclose(log);
        #200 $finish;

    end

    initial CLK <= 0;
    initial CLK_HALF <= 1;
    always #200 CLK <= ~CLK;
    always #100 CLK_HALF <= ~CLK_HALF;

    scoreboard_32 sb_32(
        .CLK(CLK),
        .ENABLE(ENABLE),
        .RESET(RESET),
        .MODO(MODO),
        .D(D_32),
        .Q_sb(Q_sb_32),
        .RCO_sb(RCO_sb_32),
        .LOAD_sb(LOAD_sb_32)
    );

    scoreboard_4 sb_4(
        .CLK(CLK),
        .ENABLE(ENABLE),
        .RESET(RESET),
        .MODO(MODO),
        .D(D_4),
        .Q_sb(Q_sb_4),
        .RCO_sb(RCO_sb_4),
        .LOAD_sb(LOAD_sb_4)
    );

endmodule