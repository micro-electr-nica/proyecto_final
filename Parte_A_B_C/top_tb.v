`timescale 1ns / 1ps
`include "contador_tb.v"
`include "counter.v"


module top_tb;

    wire CLK, ENABLE, RCO_sb, RCO_32, RCO_4, CLK_HALF, LOAD_4, LOAD_32; 
    wire [2:0] MODO;
    wire [31:0] D_32, Q_32, Q_4_tb, Q_32_tb; 
    wire [3:0] D_4, Q_4;

    counter counter_4(
        .CLK (CLK), 
        .CLK_HALF (CLK_HALF),
        .ENABLE (ENABLE),
        .RESET (RESET),
        .MODO (MODO),
        .D (D_4),
        .Q (Q_4),
        .RCO (RCO_4),
        .LOAD (LOAD_4));

    counter_32 counter_32(
		.CLK (CLK),
        .CLK_HALF (CLK_HALF),
		.ENABLE (ENABLE),
        .RESET (RESET),
		.MODO (MODO),
		.RCO (RCO_32),
		.Q (Q_32),
		.D (D_32),
        .LOAD (LOAD_32));

    contador_tb testbench(
        .CLK (CLK), 
        .CLK_HALF (CLK_HALF),
        .ENABLE (ENABLE),
        .RESET (RESET),
        .MODO (MODO),
        .D_32 (D_32),
        .D_4 (D_4),
        .Q_32 (Q_32),
        .Q_4 (Q_4),
        .RCO_32 (RCO_32),
        .RCO_4 (RCO_4),
        .LOAD_32 (LOAD_32),
        .LOAD_4 (LOAD_4));

endmodule
