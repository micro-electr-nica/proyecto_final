# Proyecto Final
## Universidad de Costa Rica
### Escuela de Ingeniería Eléctrica
#### IE-0411: Microelectrónica
##### Emmanuel Rivel Montero. B65868

## Última revisión: 
Último commit realizado el: 07/12/2020

## Abstract
En el presente proyecto se evalúa la capacidad de replicar las asignaciones previamente asignadas en el curso. Además, se propone la implementación de diferentes compuertas a nivel de layout en el software Electric así como su análisis utilizando el lenguaje Spice y visulizando su comportamiento a partir del mismo.

### Instrucciones de ejecución
Para la ejecución general de la verificación, basta con **ejecutar el comando:**
```bash
make 
```
En el subdirectorio Parte_A_B_C. De ser deseado, pueden ejecutarse los distintos pasos de la siguiente manera:
Para ejecutar **la compilación con icarus verilog**:
```bash
make iverilog
```
Para la **impresión de los resultados en consola** y generación el archivo tipo objeto:
```bash
make vvp
```
Para **desplegar GTKWave**:
```bash
make gtk
```
Para ejecutar los comandos de qflow es necesario ingresar a el subdirectorio de la Parte_D de la siguiente manera: Parte_D/tecnología/contador/
Una vez ingresado en este subdirectorio se procede a ejecutar los comandos de la siguiente manera:
Para ejecutar qflow **para obtener toda la estructura** (con los argumentos synthesize, place y route):
```bash
make init
```
Para **Obtener los resultados de temporización** utilizando vesta:
```bash
make sta
```



